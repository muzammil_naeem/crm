<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Role;

class ActivityLog extends Model
{
    protected $guarded = ['updated_at'];

    public function getCreatedAtAttribute($value)
    {
        return date('Y-m-d', strtotime($value));
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function role()
    {
        return $this->belongsTo(Role::class,'role_id','id');
    }

    public static function createLog($user = null,$msg = '',$module='',$url=null,$route_id=null,$action=null)
    {
        $role = $user->roles()->first();
        ActivityLog::create([
           'user_id' => $user->id,
           'role_id' => $role->id,
           'message' => $msg,
           'action' => $action,
           'route_id' => $route_id,
           'url' => $url,
           'module' => $module,
        ]);
    }
}
