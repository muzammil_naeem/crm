<?php

namespace App\Console\Commands;

use App\Quotation;
use Carbon\Carbon;
use Illuminate\Console\Command;

class MakeQuotationExpired extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make_quotation_expired';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Quotations Whose Expiry Date Passed, make those quotation status to expired';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $quotation = Quotation::whereIn('status',['pending','quotation-prepared','invoice-sent'])
            ->where('quotation_expiry','<', Carbon::now())
            ->where('accepted_by',null)
            ->get();

        $quotation->map(function ($quotation){
            $quotation->update(['status' => 'expired']);
        });
    }
}
