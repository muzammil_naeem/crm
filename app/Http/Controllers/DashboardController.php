<?php

namespace App\Http\Controllers;

use App\Quotation;
use Illuminate\Encryption\Encrypter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Crypt;
use \Swift_Mailer;
use \Swift_SmtpTransport as SmtpTransport;

class DashboardController extends Controller
{
    public function dashboard()
    {
        return view('AdminSide.dashboard');
    }

    public function dashboardData()
    {
        try
        {
            $user = auth()->user();
            $role = $user->roles()->first();

            $quotations = Quotation::orderBy('id','desc');
            $quotation_lists = Quotation::with(['customer','salesperson'])->orderBy('id','desc');

            $data['counts']['followup_count'] = Quotation::whereHas('followups')->where('status','invoice-sent');
            $data['counts']['pending_followup_count'] = Quotation::whereDoesntHave('followups')->where('status','invoice-sent');

            if(strtolower($role->name) != 'admin')
            {
                $quotations = $quotations->where('user_id',$user->id);
                $quotation_lists = $quotation_lists->where('user_id',$user->id);
                $data['counts']['followup_count'] = $data['counts']['followup_count']->where('user_id',$user->id);
                $data['counts']['pending_followup_count'] = $data['counts']['pending_followup_count']->where('user_id',$user->id);
            }

            $quotations = $quotations->get();
            $quotation_lists = $quotation_lists->get();
            $data['counts']['followup_count'] = $data['counts']['followup_count']->get()->count();
            $data['counts']['pending_followup_count'] = $data['counts']['pending_followup_count']->get()->count();


            $leads = $quotation_lists->whereIn('status',['completed','rejected']);
            $quotation_prepared = $quotation_lists->whereIn('status',['quotation-prepared']);
            $invoice_sent = $quotation_lists->whereIn('status',['invoice-sent']);

            $data['counts']['total_quotation_count'] = $quotations->count();
            $data['counts']['lead_count'] = $leads->count();
            $data['counts']['pending_count'] = $quotations->where('status','pending')->count();
            $data['counts']['quotation_prepared_count'] = $quotation_prepared->count();
            $data['counts']['invoice_sent_count'] = $invoice_sent->count();

            $data['leads'] =  $leads->take(5);
            $data['quotation_prepared'] =  $quotation_prepared->take(5);
            $data['invoice_sent'] =  $invoice_sent->take(5);

            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Success', 'data' => $data]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function dummyMail()
    {
        try
        {
            $encrypted = Crypt::encryptString('Ibraheem@#786');
            $decrypted_password = Crypt::decryptString($encrypted);

             $transport = new SmtpTransport('oracleholdingsllc.com', 465, 'ssl');
             $transport->setUsername('crm@oracleholdingsllc.com');
             $transport->setPassword($decrypted_password);
             $gmail = new Swift_Mailer($transport);
             \Mail::setSwiftMailer($gmail);
             \Mail::send('AdminSide.Email.resetPasswordEmail', ['token' => 'YEAs'], function ($message) {
                 $message->from('crm@oracleholdingsllc.com','Mir Ibrahim');
                 $message->to('muzammilnaeem1993@gmail.com','Check Karo');
                 $message->subject('Check Karo');
             });

            // $transport = new SmtpTransport('hitechhub.tech', 465, 'ssl');
            // $transport->setUsername('muzammil@hitechhub.tech');
            // $transport->setPassword('Muz@mmil123');
            // $gmail = new Swift_Mailer($transport);
            // \Mail::setSwiftMailer($gmail);
            // \Mail::send('AdminSide.Email.resetPasswordEmail', ['token' => 'YEAs'], function ($message) {
            //     $message->from('muzammil@hitechhub.tech','Muzammil Naeem');
            //     $message->to('muzammilnaeem1993@gmail.com','Check Karo');
            //     $message->subject('Check Karo');
            // });

            // $transport = new SmtpTransport('hitechhub.tech', 465, 'ssl');
            // $transport->setUsername('_mainaccount@hitechhub.tech');
            // $transport->setPassword('P5uqyYVkf5Pc');
            // $gmail = new Swift_Mailer($transport);
            // \Mail::setSwiftMailer($gmail);
            // \Mail::send('AdminSide.Email.resetPasswordEmail', ['token' => 'YEAs'], function ($message) {
            //     $message->from('_mainaccount@hitechhub.tech','HitechHub Tech');
            //     $message->to('muzammilnaeem1993@gmail.com','Check Karo');
            //     $message->subject('Check Karo');
            // });

        } catch (\Exception $ex) {
            echo 'Not Sended.';
            \Log::info($ex);
            return;
        }

        echo 'Done.';

    }
}
