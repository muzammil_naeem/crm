<?php

namespace App\Http\Controllers\WebController;

use App\ActivityLog;
use App\Http\Requests\CustomerFormValidation;
use App\User;
use Illuminate\Routing\Controller;

class ActivityLogController extends Controller
{
    public function index()
    {
        try
        {
            $user = auth()->user();
            $role = $user->roles()->first();

            $data['activity_logs'] = ActivityLog::with(['user'])->orderBy('id','desc');

            if(strtolower($role->name) != 'admin')
                $data['activity_logs'] = $data['activity_logs']->where('user_id',$user->id);

            $data['activity_logs'] = $data['activity_logs']->get();

            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Success', 'data' => $data]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function create()
    {
        try
        {

            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Create Data Sent', 'data' => new \stdClass()]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function store(CustomerFormValidation $request)
    {
        try
        {
            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Customer Created Successfully', 'data' => new \stdClass()]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function show($id)
    {
        try
        {

            return response()->json(['code' => 200, 'status' => 'success' ,'message' => '', 'data' => new \stdClass()]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function edit($id)
    {
        try
        {

            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Create Data Sent', 'data' => new \stdClass()]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function update(CustomerFormValidation $request, $id)
    {
        try
        {
            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Data Updated Successfully', 'data' => new \stdClass()]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function destroy($id)
    {
        try
        {
            $activity_log = ActivityLog::find($id);
            $activity_log_obj = $activity_log;
            $activity_log->delete();

            $msg = 'Activity Log Deleted by '.auth()->user()->full_name;
            ActivityLog::createLog(auth()->user(),$msg,'activity_log',null,$activity_log_obj->id,'deleted');

            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Activity Log Deleted Successfully', 'data' => new \stdClass()]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

}
