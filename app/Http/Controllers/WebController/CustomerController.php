<?php

namespace App\Http\Controllers\WebController;

use App\ActivityLog;
use App\Company;
use App\Http\Requests\CustomerFormValidation;
use App\User;
use Illuminate\Routing\Controller;

class CustomerController extends Controller
{
    public function index()
    {
        try
        {
            $user = auth()->user();
            $role = $user->roles()->first();

            $data['customers'] = User::with('salesperson')->where('user_type','customer')->orderBy('id','desc');

            if(strtolower($role->name) != 'admin')
                $data['customers'] = $data['customers']->where('parent_id',$user->id);

            $data['customers'] = $data['customers']->get();

            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Success', 'data' => $data]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function create()
    {
        try
        {
            $companies = Company::select('name')->get();
            $data['salesperson'] = auth()->user();
            $data['customer_no'] = uniqid();
            $data['companies'] = $companies;

            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Create Data Sent', 'data' => $data]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function store(CustomerFormValidation $request)
    {
        try
        {
            $user = new User();
            $user->first_name = $request->first_name;
            $user->last_name = $request->last_name;
            $user->email = $request->email;
            $user->user_type = 'customer';
            $user->password = '';
            $user->contact_no = $request->contact_no;
            $user->customer_no = $request->customer_no;
            $user->company_name = $request->company_name;
            $user->address = $request->address;
            $user->source = $request->source;
            $user->platform = $request->source == 'other-platform' ? $request->platform : null;
            $user->parent_id = auth()->user()->id;
            $user->save();

            $this->create_company_if_not_exist($user->company_name);

            $msg = 'New Customer '. $user->full_name .' has been Created by '.auth()->user()->full_name;
            $url = '/customers/edit/'.$user->id;
            ActivityLog::createLog(auth()->user(),$msg,'customer',$url,$user->id,'created');

            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Customer Created Successfully', 'data' => new \stdClass()]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function show($id)
    {
        try
        {


            return response()->json(['code' => 200, 'status' => 'success' ,'message' => '', 'data' => new \stdClass()]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function edit($id)
    {
        try
        {
            $companies = Company::select('name')->get();

            $data['user'] = User::with('salesperson')->find($id);
            $data['companies'] = $companies;

            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Create Data Sent', 'data' => $data]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function update(CustomerFormValidation $request, $id)
    {
        try
        {
            $user = User::find($id);
            $user->first_name = $request->first_name;
            $user->last_name = $request->last_name;
            $user->email = $request->email;
            $user->customer_no = $request->customer_no;
            $user->contact_no = $request->contact_no;
            $user->company_name = $request->company_name;
            $user->address = $request->address;
            $user->source = $request->source;
            $user->platform = $request->source == 'other-platform' ? $request->platform : null;
            $user->save();

            $this->create_company_if_not_exist($user->company_name);

            $msg = 'Customer '.$user->full_name.' has been Updated by '.auth()->user()->full_name;
            $url = '/customers/edit/'.$user->id;
            ActivityLog::createLog(auth()->user(),$msg,'customer',$url,$user->id,'updated');

            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Data Updated Successfully', 'data' => new \stdClass()]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function destroy($id)
    {
        try
        {
            $user = User::find($id);
            $user_obj = $user;
            $user->delete();

            $msg = 'Customer '.$user_obj->full_name.' Deleted by '.auth()->user()->full_name;
            ActivityLog::createLog(auth()->user(),$msg,'customer',null,$user->id,'deleted');

            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Sales Person Deleted Successfully', 'data' => new \stdClass()]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function create_company_if_not_exist($company_name)
    {
        $company = Company::where('name',$company_name)->first();
        if(empty($company))
            Company::create(['name' => $company_name]);
    }

}
