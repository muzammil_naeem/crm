<?php

namespace App\Http\Controllers\WebController;

use App\ActivityLog;
use App\Http\Requests\CustomerFormValidation;
use App\Part;
use App\User;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;

class GlobalController extends Controller
{
    public function getCustomer($id)
    {
        try
        {
            $data['customer'] = User::with('salesperson')->find($id);

            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Success', 'data' => $data]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    /*AJAX request*/
    public function getCustomersAjax(Request $request){
        try
        {
            $search = $request->search;
            $limit = 50;

            if($search == ''){
                $customers = User::where('user_type','customer')->orderby('first_name','asc')->select('id','first_name','last_name','email','company_name')->limit($limit)->get();

                $response = array();
                foreach($customers as $customer){
                    $response[] = array(
                        "id"=>$customer->id,
                        "text"=> $customer->first_name.' '.$customer->last_name,
                    );
                }

            }else{

                $customers = User::where('user_type','customer')->orderby('first_name','asc')->select('id','first_name','last_name','email','company_name')->where('first_name', 'like', '%' .$search . '%')->orwhere('last_name', 'like', '%' .$search . '%')->limit($limit)->get();
                if($customers->isNotEmpty())
                {
                    \Log::info($customers);
                    $response = array();
                    foreach($customers as $customer){
                        $response[] = array(
                            "id"=>$customer->id,
                            "text"=> $customer->first_name.' '.$customer->last_name.' - '.$customer->email.' - '.$customer->company_name,
                        );
                    }
                }
                else{
                    $customers = User::where('user_type','customer')->orderby('first_name','asc')->select('id','first_name','last_name','email','company_name')->where('email', 'like', '%' .$search . '%')->limit($limit)->get();
                    \Log::info($customers);
                    if($customers->isNotEmpty())
                    {
                        \Log::info('email');
                        $response = array();
                        foreach($customers as $customer){
                            $response[] = array(
                                "id"=>$customer->id,
                                "text"=> $customer->first_name.' '.$customer->last_name.' - '.$customer->email.' - '.$customer->company_name,
                            );
                        }
                    }
                    else
                    {
                        $customers = User::where('user_type','customer')->orderby('first_name','asc')->select('id','first_name','last_name','email','company_name')->where('company_name', 'like', '%' .$search . '%')->limit($limit)->get();
                        if($customers->isNotEmpty())
                        {
                            $response = array();
                            foreach($customers as $customer){
                                $response[] = array(
                                    "id"=>$customer->id,
                                    "text"=> $customer->first_name.' '.$customer->last_name.' - '.$customer->email.' - '.$customer->company_name
                                );
                            }
                        }
                        else
                        {
                            $customers = User::where('user_type','customer')->orderby('first_name','asc')->select('id','first_name','last_name','email','customer_no')->where('customer_no', 'like', '%' .$search . '%')->limit($limit)->get();
                            if($customers->isNotEmpty())
                            {
                                $response = array();
                                foreach($customers as $customer){
                                    $response[] = array(
                                        "id"=>$customer->id,
                                        "text"=> $customer->customer_no.' - '.$customer->first_name.' '.$customer->last_name.' - '.$customer->email,
                                    );
                                }
                            }
                        }
                    }
                }
            }
            return response()->json($response);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function getPart($id)
    {
        try
        {
            $data['part'] = Part::find($id);

            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Success', 'data' => $data]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    /*AJAX request*/
    public function getPartsAjax(Request $request){
        try
        {
            $search = $request->search;
            $limit = 50;

            if($search == ''){
                $parts = Part::orderby('part_no','asc')->select('id','part_no')->limit($limit)->get();

                $response = array();
                foreach($parts as $part){
                    $response[] = array(
                        "id"=>$part->id,
                        "text"=>'#'.$part->part_no,
                    );
                }

            }else{
                $parts = Part::orderby('part_no','asc')->select('id','part_no')->where('part_no', 'like', '%' .$search . '%')->limit($limit)->get();
                if($parts->isNotEmpty())
                {
                    $response = array();
                    foreach($parts as $part){
                        $response[] = array(
                            "id"=>$part->id,
                            "text"=>'#'.$part->part_no,
                        );
                    }
                }
                else
                {
                    $parts = Part::orderby('part_no','asc')->select('id','name')->where('name', 'like', '%' .$search . '%')->limit($limit)->get();
                    if($parts->isNotEmpty())
                    {
                        $response = array();
                        foreach($parts as $part){
                            $response[] = array(
                                "id"=>$part->id,
                                "text"=>$part->name,
                            );
                        }
                    }
                }
            }

            return response()->json($response);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function allPartsDatatable(Request $request)
    {
        try
        {
            $totalData = Part::count();

            $totalFiltered = $totalData;

            $limit = $request->input('length');
            $start = $request->input('start');
            $order = 'name';
            $dir = $request->input('order.0.dir');

            if(empty($request->input('search.value')))
            {
                $parts = Part::offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir)
                    ->get();
            }
            else {
                $search = $request->input('search.value');

                $parts =  Part::where('name','LIKE',"%{$search}%")
                    ->orWhere('part_no', 'LIKE',"%{$search}%")
                    ->orWhere('created_at', 'LIKE',"%{$search}%")
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir)
                    ->get();

                $totalFiltered = Part::where('name','LIKE',"%{$search}%")
                    ->orWhere('part_no', 'LIKE',"%{$search}%")
                    ->orWhere('created_at', 'LIKE',"%{$search}%")
                    ->count();
            }

            $data = array();
            if(!empty($parts))
            {
                foreach ($parts as $key => $part)
                {
                    $edit = '#/parts/edit/'.$part->id;

                    $nestedData['sr_no'] = $key + 1;
                    $nestedData['name'] = $part->name;
                    $nestedData['part_no'] = $part->part_no;
                    $nestedData['created_at'] = $part->created_at;
                    $nestedData['action'] = "&emsp;<a href='$edit' class='btn btn-icon btn-info tooltip-info tooltip-vue' title='Edit Parts'><i class='la la-pencil text-white'></i></a>";
                    $data[] = $nestedData;

                }
            }

            $json_data = array(
                "draw"            => intval($request->input('draw')),
                "recordsTotal"    => intval($totalData),
                "recordsFiltered" => intval($totalFiltered),
                "data"            => $data
            );

            return response()->json($json_data);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function activityLogDatatable(Request $request)
    {
        try
        {
            $user = auth()->user();
            $role = $user->roles()->first();

            $totalData = strtolower($role->name) != 'admin' ? ActivityLog::where('user_id',$user->id)->count() : ActivityLog::count();

            $totalFiltered = $totalData;

            $limit = $request->input('length');
            $start = $request->input('start');
            $order = 'created_at';
            $dir = 'desc';

            if(empty($request->input('search.value')))
            {
                $activity_log = ActivityLog::offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir);
            }
            else {
                $search = $request->input('search.value');

                $activity_log =  ActivityLog::where('message', 'LIKE',"%{$search}%")
                    ->orWhere('created_at', 'LIKE',"%{$search}%")
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir);

                $totalFiltered = ActivityLog::where('message','LIKE',"%{$search}%")
                    ->orWhere('created_at', 'LIKE',"%{$search}%");

                    if(strtolower($role->name) != 'admin')
                        $totalFiltered = $totalFiltered->where('user_id',$user->id);

                $totalFiltered = $totalFiltered->count();
            }

            if(strtolower($role->name) != 'admin')
                $activity_log = $activity_log->where('user_id',$user->id);

            $activity_log = $activity_log->get();

            $data = array();
            if(!empty($activity_log))
            {
                foreach ($activity_log as $key => $log)
                {
                    $edit = '#/parts/edit/'.$log->id;

                    $nestedData['sr_no'] = $key + 1;
                    $nestedData['name'] = $log->user->full_name;
                    $nestedData['message'] = $log->message;
                    $nestedData['created_at'] = $log->created_at;
                    $nestedData['action'] = "&emsp;<a class='btn btn-icon btn-danger deleteActivityLog' data-index='$key' data-id='$log->id'><i class='la la-trash text-white'></i></a>";
                    $data[] = $nestedData;
                }
            }

            $json_data = array(
                "draw"            => intval($request->input('draw')),
                "recordsTotal"    => intval($totalData),
                "recordsFiltered" => intval($totalFiltered),
                "data"            => $data
            );

            return response()->json($json_data);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }
}
