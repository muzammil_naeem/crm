<?php

namespace App\Http\Controllers\WebController;

use App\ActivityLog;
use App\Jobs\CreatePDFandSendEmail;
use App\Jobs\SendCustomerInvoiceAcceptanceEmail;
use App\Quotation;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use PDF;
use File;
use Mail;
use Session;
use Illuminate\Routing\Controller;
use \Swift_Mailer;
use \Swift_SmtpTransport as SmtpTransport;

class InvoiceController extends Controller
{

    public function index()
    {
        try
        {
            $user = auth()->user();
            $role = $user->roles()->first();

            $data['quotations'] = Quotation::with(['customer','salesperson'])->where('status','invoice-sent');
            $data['salespersons'] = User::where('user_type','salesperson')->orderBy('id','Desc')->get();

            if(strtolower($role->name) != 'admin')
                $data['quotations'] = $data['quotations']->where('user_id',$user->id);

            $data['quotations'] = $data['quotations']->orderBy('id','desc')->get();

            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Success', 'data' => $data]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function sendInvoice($id)
    {
        try
        {
            $quotation = Quotation::with(['customer','salesperson','parts'])->find($id);
            $quotation->status = 'invoice-sent';

            $data['quotation'] = $quotation;
            $data['customer'] = $data['quotation']['customer'];
            $data['salesperson'] = $data['quotation']['salesperson'];

            $data['quotation']['parts']->map(function ($part)
            {
                $part['quantity'] = $part['pivot']['quantity'];
                $part['condition'] = $part['pivot']['condition'];
                $part['price'] = $part['pivot']['quoted_price'];
                $part['grand_extended_price'] = $part['pivot']['grand_extended_price'];
            });

            $pdf = PDF::loadView('AdminSide.Invoice.downloadInvoice', compact('data'))->setPaper(array(0,0,780,1440),'portrait');
            $basepath = storage_path("app/public/invoices/".$data['quotation']['id']);
            if (!file_exists($basepath)) {
                File::makeDirectory($basepath, 0777, true);
            }
            $pdf->save($basepath.'/invoice.pdf');

//            CreatePDFandSendEmail::dispatch($data);

            try
            {
//                $transport = new SmtpTransport($data['salesperson']['host'], $data['salesperson']['port'], $data['salesperson']['encryption']);
//                $transport->setUsername($data['salesperson']['email_username']);
//                $transport->setPassword($data['salesperson']['email_password']);
//                $mail = new Swift_Mailer($transport);
//                Mail::setSwiftMailer($mail);
                Mail::send('AdminSide.Email.generateInvoiceEmail', ['data' => $data], function ($message) use ($data) {
                    $message->subject('Customer Invoice');
                    $message->to($data['quotation']['email']);
                    $message->from($data['salesperson']['email_username'], $data['salesperson']['full_name']);
                });

            } catch (\Exception $ex) {
                \Log::info($ex->getMessage());
                return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => 'Your Email Settings Are Incorrect.','data' => new \stdClass()]);
            }

            $quotation->save();

            $customer_name = $data['customer']['first_name'].' '.$data['customer']['last_name'];

            $msg = 'Invoice Has Been Sent to Customer '.$customer_name.' Against Quotation #'.$quotation->quote_no.' by '.auth()->user()->full_name;
            $url = '/invoice/'.$quotation->id;
            ActivityLog::createLog(auth()->user(),$msg,'invoice',$url,$quotation->id,'created');

            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Invoice Sent Successfully', 'data' => new \stdClass()]);

        } catch (\Exception $ex) {
            \Log::info($ex->getMessage());
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function viewInvoiceCustomer($id)
    {
        $data['quotation'] = Quotation::with(['customer','salesperson','parts'])->find($id);
        $data['customer'] = $data['quotation']['customer'];
        $data['salesperson'] = $data['quotation']['salesperson'];

        $data['quotation']['parts']->map(function ($part)
        {
            $part['quantity'] = $part['pivot']['quantity'];
            $part['condition'] = $part['pivot']['condition'];
            $part['price'] = $part['pivot']['quoted_price'];
            $part['grand_extended_price'] = $part['pivot']['grand_extended_price'];
        });

        return view('AdminSide.Invoice.view',compact('data'));
    }

    public function downloadInvoicePdf($id)
    {
        return $this->downloadCustomerInvoice($id);
    }

    public function downloadInvoicePdfSalesPerson($id)
    {
        return $this->downloadCustomerInvoice($id);
    }

    public function downloadCustomerInvoice($id)
    {
        $basepath = storage_path("app/public/invoices/".$id."/invoice.pdf");
        return response()->download($basepath);
    }

    public function downloadPurchaseOrder($id)
    {
        $quotation = Quotation::find($id);
        $basepath = storage_path("app/public/purchase_order/".$id."/".$quotation->purchase_order_file_name);
        return response()->download($basepath);
    }

    public function acceptInvoiceCustomer(Request $request,$id)
    {
        try
        {
            $quotation = Quotation::with(['customer','salesperson','parts'])->find($id);

            $data['quotation'] = $quotation;
            $data['customer'] = $data['quotation']['customer'];
            $data['salesperson'] = $data['quotation']['salesperson'];

            /*SendCustomerInvoiceAcceptanceEmail::dispatch($data);*/

            $basepath = storage_path("app/public/invoices/".$data['quotation']['id']."/invoice.pdf");
            \Mail::send('AdminSide.Email.customerAcceptanceEmail', ['data' => $data], function ($message) use ($data,$basepath) {
                $message->subject('Customer Invoice Acceptance');
                $message->to($data['salesperson']['email']);
                $message->from(config()->get('mail.username'), $data['customer']['full_name']);
                $message->attach($basepath);
            });

            $quotation->accepted_by = $quotation->customer_id;
            $quotation->comments = $request->comments ? $request->comments : null;
            $quotation->save();

            Session::flash('message', 'Your Quotation Accepted Successfully !!!');

            return redirect()->back();

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function acceptInvoiceSalesPerson($id)
    {
        try
        {
            $quotation = Quotation::find($id);
            $quotation->accepted_by = $quotation->user_id;
            $quotation->save();

            $msg = 'Quotation #'.$quotation->quote_no.' Has Been Accepted by Sales Person '.auth()->user()->full_name;
            $url = '/invoice/'.$quotation->id;
            ActivityLog::createLog(auth()->user(),$msg,'invoice',$url,$quotation->id,'updated');

            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Invoice Accepted Successfully', 'data' => new \stdClass()]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function customerFollowUp(Request $request,$id)
    {
        try
        {
            $quotation = Quotation::find($id);
            $attributes[auth()->user()->id] = [
                'comments' => $request->comments,
                'followup_email' => $request->email ? $request->email : 0,
                'followup_call' => $request->call ? $request->call : 0,
            ];

            $quotation->followups()->attach($attributes);

            $msg = auth()->user()->full_name.' Has Done Follow Up Against Quotation #'.$quotation->quote_no;
            $url = '/invoice/'.$quotation->id;
            ActivityLog::createLog(auth()->user(),$msg,'invoice',$url,$quotation->id,'updated');

            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Follow Up Status Updated', 'data' => new \stdClass()]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function followUpListing($id)
    {
        try
        {
            $data['quotation'] = Quotation::with(['followups','customer'])->find($id);

            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Follow Up Status Updated', 'data' => $data]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function uploadPurchaseOrder(Request $request,$id)
    {
        try
        {
            if (!$request->hasFile('file')) {
                return response()->json(['code' => 422, 'status' => 'failure' ,'message' => 'Please Upload the Purchase Order', 'data' => new \stdClass()]);
            }

            $document = $request->file('file');
            $extension = $document->getClientOriginalExtension();

            $file_name = 'purchase_order.'.$extension;
            $destinationPath = $basepath = storage_path("app/public/purchase_order/".$id."/");

            $upload_success = $document->move($destinationPath, $file_name);
            if($upload_success)
            {
                $quotation = Quotation::find($id);
                $quotation->purchase_order_uploaded = 1;
                $quotation->purchase_order_file_name = $file_name;
                $quotation->save();

                $msg = 'Purchase Order Has Been Uploaded Against Quotation #'.$quotation->quote_no.' by '.auth()->user()->full_name;
                $url = '/invoice/'.$quotation->id;
                ActivityLog::createLog(auth()->user(),$msg,'invoice',$url,$quotation->id,'uploaded');

                return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Purchase Order Uploaded Successfully !!!', 'data' => new \stdClass()]);
            }
            else
            {
                return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Purchase Order Not Uploaded.', 'data' => new \stdClass()]);
            }

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function markQuotationComplete($id)
    {
        try
        {
            $quotation = Quotation::find($id);
            $quotation->status = 'completed';
            $quotation->save();

            $msg = 'Quotation #'.$quotation->quote_no.' Has Been Marked Completed by '.auth()->user()->full_name;
            $url = '/invoice/'.$quotation->id;
            ActivityLog::createLog(auth()->user(),$msg,'invoice',$url,$quotation->id,'completed');

            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Quotation Successfully Marked Completed !!!', 'data' => new \stdClass()]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function pendingFollowups()
    {
        try
        {
            $user = auth()->user();
            $role = $user->roles()->first();

            $data['quotations'] = Quotation::with(['customer','salesperson'])->whereDoesntHave('followups')->where('status','invoice-sent');
            $data['salespersons'] = User::where('user_type','salesperson')->orderBy('id','Desc')->get();

            if(strtolower($role->name) != 'admin')
                $data['quotations'] = $data['quotations']->where('user_id',$user->id);

            $data['quotations'] = $data['quotations']->orderBy('id','desc')->get();

            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Success', 'data' => $data]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }
}
