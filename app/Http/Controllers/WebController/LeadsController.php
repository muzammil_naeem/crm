<?php

namespace App\Http\Controllers\WebController;

use App\ActivityLog;
use App\Http\Requests\CustomerFormValidation;
use App\Http\Requests\PrepareQuotationFormValidation;
use App\Http\Requests\RequestForQuotationFormValidation;
use App\Part;
use App\Quotation;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class LeadsController extends Controller
{
    public function index()
    {
        try
        {
            $user = auth()->user();
            $role = $user->roles()->first();

            $data['quotations'] = Quotation::with(['customer','salesperson','accepted_by'])->whereIn('status',['completed','rejected']);
            $data['salespersons'] = User::where('user_type','salesperson')->orderBy('id','Desc')->get();

            if(strtolower($role->name) != 'admin')
                $data['quotations'] = $data['quotations']->where('user_id',$user->id);

            $data['quotations'] = $data['quotations']->orderBy('updated_at','desc')->get();

            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Success', 'data' => $data]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function create()
    {
        try
        {

            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Create Data Sent', 'data' => new \stdClass()]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function store(RequestForQuotationFormValidation $request)
    {
        try
        {

            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Prepared Quotation Successfully', 'data' => new \stdClass()]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function show($id)
    {
        try
        {

            return response()->json(['code' => 200, 'status' => 'success' ,'message' => '', 'data' => new \stdClass()]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function edit($id)
    {
        try
        {

            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Edit Data Sent', 'data' => new \stdClass()]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function update(RequestForQuotationFormValidation $request, $id)
    {
        try
        {
            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Request For Quotation Updated Successfully', 'data' => new \stdClass()]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function destroy($id)
    {
        try
        {

            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Quotation Deleted Successfully', 'data' => new \stdClass()]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }
}
