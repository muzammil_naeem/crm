<?php

namespace App\Http\Controllers\WebController;

use App\ActivityLog;
use App\Http\Requests\CustomerFormValidation;
use App\Http\Requests\PartsFormValidation;
use App\Part;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class PartsController extends Controller
{
    public function index()
    {
        try
        {
//            $data['parts'] = Part::orderBy('id','desc')->get();

            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Success', 'data' =>  new \stdClass()]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function create()
    {
        try
        {
            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Create Data Sent', 'data' =>  new \stdClass()]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function store(PartsFormValidation $request)
    {
        try
        {
            $part = new Part();
            $part->name = $request->name;
            $part->part_no = $request->part_no;
            $part->save();

            $msg = 'New Part '. $part->name .' has been Created by '.auth()->user()->full_name;
            $url = '/parts/edit/'.$part->id;
            ActivityLog::createLog(auth()->user(),$msg,'part',$url,$part->id,'created');

            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Part Created Successfully', 'data' => new \stdClass()]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function show($id)
    {
        try
        {


            return response()->json(['code' => 200, 'status' => 'success' ,'message' => '', 'data' => new \stdClass()]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function edit($id)
    {
        try
        {
            $data['part'] = Part::find($id);

            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Edit Data Sent', 'data' =>  $data]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function update(PartsFormValidation $request, $id)
    {
        try
        {
            $part = Part::find($id);
            $part->part_no = $request->part_no;
            $part->name = $request->name;
            $part->save();

            $msg = $part->name .' Part has been Updated by '.auth()->user()->full_name;
            $url = '/parts/edit/'.$part->id;
            ActivityLog::createLog(auth()->user(),$msg,'part',$url,$part->id,'updated');

            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Data Updated Successfully', 'data' => new \stdClass()]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function destroy($id)
    {
        try
        {

            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Parts Deleted Successfully', 'data' => new \stdClass()]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function downloadPartsCsv()
    {
        return response()->download(public_path('parts.csv'));
    }

    public function importParts(Request $request)
    {
        if ($request->hasFile('file')) {
            $data = [];
            $file_ext = $request->file('file')->getClientOriginalExtension();
            if ($file_ext !== 'csv') {
                return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => 'Please import file with csv extension!','data' => new \stdClass()]);
            }
            $sheet_values = [];
            $fileData = fopen($request->file('file'), 'r');
            while (($line = fgetcsv($fileData)) !== FALSE) {
                $sheet_values[] = $line;
            }

            unset($sheet_values[0]);
            $sheet_values = \Illuminate\Support\Collection::make($sheet_values);
            $sheet_values->chunk(500)->each(function ($parts) use (&$data) {
                foreach ($parts as $key => $part) {
                    $part_obj = Part::where('part_no',strtolower($part[1]))->first();
                    if(empty($part_obj))
                        $data['parts'][] = Part::create(['name' => $part[0],'part_no' => $part[1]]);
                }
            });

            $msg = 'Parts Bulk Uploaded by '.auth()->user()->full_name;
            ActivityLog::createLog(auth()->user(),$msg,'part',null,null,'created');

            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Parts Uploaded Successfully', 'data' => $data]);
        }
        else {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => 'Please import file with csv extension!','data' => new \stdClass()]);
        }
    }

}
