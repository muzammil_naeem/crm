<?php

namespace App\Http\Controllers\WebController;

use App\ActivityLog;
use App\Http\Requests\CustomerFormValidation;
use App\Http\Requests\PrepareQuotationFormValidation;
use App\Http\Requests\RequestForQuotationFormValidation;
use App\Part;
use App\Quotation;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class PriceComparisonController extends Controller
{
    public function index()
    {
        try
        {
            $user = auth()->user();
            $role = $user->roles()->first();

            $data['quotations'] = Quotation::with(['customer','salesperson'])->where('status','quotation-prepared');
            $data['salespersons'] = User::where('user_type','salesperson')->orderBy('id','Desc')->get();

            if(strtolower($role->name) != 'admin')
                $data['quotations'] = $data['quotations']->where('user_id',$user->id);

            $data['quotations'] = $data['quotations']->orderBy('id','desc')->get();

            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Success', 'data' => $data]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function create()
    {
        try
        {

            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Create Data Sent', 'data' => new \stdClass()]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function store(RequestForQuotationFormValidation $request)
    {
        try
        {

            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Prepared Quotation Successfully', 'data' => new \stdClass()]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function show($id)
    {
        try
        {

            return response()->json(['code' => 200, 'status' => 'success' ,'message' => '', 'data' => new \stdClass()]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function edit($id)
    {
        try
        {
            $data['quotation'] = Quotation::with(['customer','salesperson','parts','rejection_answers.question'])->find($id);
            if(empty($data['quotation']))
            {
                return response()->json(['code' => 422, 'status' => 'success' ,'message' => 'Quotation Not Exitst Against this Id', 'data' => $data]);
            }

            $data['customers'] = User::where('user_type','customer')->get();
            $data['salesperson'] = $data['quotation']['salesperson'];
            $data['rejection_answers'] = $data['quotation']['rejection_answers'];


            $data['quotation']['parts']->map(function ($part)
            {
                $part['quantity'] = $part['pivot']['quantity'];
                $part['condition'] = $part['pivot']['condition'];
                $part['price'] = $part['pivot']['price'];
                $part['extended_price'] = $part['pivot']['extended_price'];
                $part['grand_extended_price'] = $part['pivot']['grand_extended_price'];
                $part['fst_cheapest_price'] = $part['pivot']['fst_cheapest_price'];
                $part['fst_company_name'] = $part['pivot']['fst_company_name'];
                $part['fst_product_url'] = $part['pivot']['fst_product_url'];
                $part['fst_stock'] = $part['pivot']['fst_stock'];
                $part['fst_lead_week'] = $part['pivot']['fst_lead_week'];
                $part['fst_lead_day'] = $part['pivot']['fst_lead_day'];
                $part['fst_lead_time_type'] = $part['pivot']['fst_lead_time_type'];
                $part['fst_lead_time_value'] = $part['pivot']['fst_lead_time_value'];
                $part['fst_additional_info'] = $part['pivot']['fst_additional_info'];
                $part['second_cheapest_price'] = $part['pivot']['second_cheapest_price'];
                $part['second_company_name'] = $part['pivot']['second_company_name'];
                $part['second_product_url'] = $part['pivot']['second_product_url'];
                $part['second_stock'] = $part['pivot']['second_stock'];
                $part['second_lead_week'] = $part['pivot']['second_lead_week'];
                $part['second_lead_day'] = $part['pivot']['second_lead_day'];
                $part['second_lead_time_type'] = $part['pivot']['second_lead_time_type'];
                $part['second_lead_time_value'] = $part['pivot']['second_lead_time_value'];
                $part['second_additional_info'] = $part['pivot']['second_additional_info'];
                $part['third_cheapest_price'] = $part['pivot']['third_cheapest_price'];
                $part['third_company_name'] = $part['pivot']['third_company_name'];
                $part['third_product_url'] = $part['pivot']['third_product_url'];
                $part['third_stock'] = $part['pivot']['third_stock'];
                $part['third_lead_week'] = $part['pivot']['third_lead_week'];
                $part['third_lead_day'] = $part['pivot']['third_lead_day'];
                $part['third_lead_time_type'] = $part['pivot']['third_lead_time_type'];
                $part['third_lead_time_value'] = $part['pivot']['third_lead_time_value'];
                $part['third_additional_info'] = $part['pivot']['third_additional_info'];
                $part['quoted_price'] = $part['pivot']['quoted_price'];
                $part['source_price'] = $part['pivot']['source_price'];
                $part['source_company'] = $part['pivot']['source_company'];
                $part['custom_note'] = $part['pivot']['custom_note'];
            });

            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Edit Data Sent', 'data' => $data]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function update(RequestForQuotationFormValidation $request, $id)
    {
        try
        {
            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Request For Quotation Updated Successfully', 'data' => new \stdClass()]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function destroy($id)
    {
        try
        {

            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Quotation Deleted Successfully', 'data' => new \stdClass()]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function createPriceComparison(PrepareQuotationFormValidation $request,$id)
    {
        try
        {
            $quotation = Quotation::find($id);
            $attributes = [];
            foreach ($request->part_id as $key => $part)
            {
                $attributes[$part] = [
                    'fst_cheapest_price' => $request->fst_cheapest_price[$key],
                    'fst_company_name' => $request->fst_company_name[$key],
                    'fst_product_url' => $request->fst_product_url[$key],
                    'fst_stock' => $request->fst_stock[$key],
                    'fst_lead_time_type' => $request->fst_lead_time_type[$key],
                    'fst_lead_time_value' => $request->fst_lead_time_value[$key],
                    'fst_additional_info' => $request->fst_additional_info[$key],
                    'second_cheapest_price' => $request->second_cheapest_price[$key],
                    'second_company_name' => $request->second_company_name[$key],
                    'second_product_url' => $request->second_product_url[$key],
                    'second_stock' => $request->second_stock[$key],
                    'second_lead_time_type' => $request->second_lead_time_type[$key],
                    'second_lead_time_value' => $request->second_lead_time_value[$key],
                    'second_additional_info' => $request->second_additional_info[$key],
                    'third_cheapest_price' => $request->third_cheapest_price[$key],
                    'third_company_name' => $request->third_company_name[$key],
                    'third_product_url' => $request->third_product_url[$key],
                    'third_stock' => $request->third_stock[$key],
                    'third_lead_time_type' => $request->third_lead_time_type[$key],
                    'third_lead_time_value' => $request->third_lead_time_value[$key],
                    'third_additional_info' => $request->third_additional_info[$key],
                    'quoted_price' => $request->quoted_price[$key],
                    'grand_extended_price' => $request->grand_extended_price[$key],
                    'source_price' => $request->source_price[$key],
                    'source_company' => $request->source_company[$key],
                    'custom_note' => $request->custom_note[$key],
                ];
            }

            $quotation->parts()->sync($attributes);
            $quotation->status = 'quotation-prepared';
            $quotation->save();

            $msg = 'Price Comparison For Quotation #'.$quotation->quote_no.' has been Updated by '.auth()->user()->full_name;
            $url = '/viewPriceComparison/'.$quotation->id;
            ActivityLog::createLog(auth()->user(),$msg,'price-comparison',$url,$quotation->id,'updated');

            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Prepared Quotation Successfully', 'data' => new \stdClass()]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function reviewQuotation($id)
    {
        try
        {
            $data['quotation'] = Quotation::with(['customer','salesperson','parts'])->find($id);
            $data['customers'] = User::where('user_type','customer')->get();
            $data['salesperson'] = $data['quotation']['salesperson'];

            $data['quotation']['parts']->map(function ($part)
            {
                $part['quantity'] = $part['pivot']['quantity'];
                $part['condition'] = $part['pivot']['condition'];
                $part['price'] = $part['pivot']['quoted_price'];
                $part['grand_extended_price'] = $part['pivot']['grand_extended_price'];
            });

            $data['quotation']['total'] = array_sum($data['quotation']['parts']->pluck('grand_extended_price')->toArray());

            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Prepared Quotation Data Sent Successfully', 'data' => $data]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function submitQuotation(Request $request,$id)
    {
        try
        {
            $quotation = Quotation::find($id);
            $quotation->total = $request->total;
            $quotation->shipment_cost = $request->shipment_cost;
            $quotation->tax_percentage = $request->tax_percentage;
            $quotation->credit_card_percentage = $request->credit_card_percentage;
            $quotation->grand_total = $request->grand_total;
            $quotation->save();

            $msg = 'Shipment Cost for Quotation #'.$quotation->quote_no.' has been Entered by '.auth()->user()->full_name;
            $url = '/viewPriceComparison/'.$quotation->id;
            ActivityLog::createLog(auth()->user(),$msg,'price-comparison',$url,$quotation->id,'created');

            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Submit Quotation Successfully', 'data' => new \stdClass()]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function assignSalesPerson(Request $request,$id)
    {
        try
        {
            $quotation = Quotation::with('salesperson')->find($id);

            $old_salesperson = $quotation['salesperson'];
            $old_salesperson_name  = $old_salesperson['first_name'].' '.$old_salesperson['last_name'];

            $quotation->user_id = $request->salesperson_id;
            $quotation->save();
            $quotation->refresh();

            $data['salesperson'] = $quotation->salesperson;
            $salesperson_name  = $data['salesperson']['first_name'].' '.$data['salesperson']['last_name'];

            $msg = 'Sales Person '.$old_salesperson_name.' Has Been Remove by '.auth()->user()->full_name.' against Quotation #'.$quotation->quote_no;
            $url = '/viewPriceComparison/'.$quotation->id;
            ActivityLog::createLog(auth()->user(),$msg,'price-comparison',$url,$quotation->id,'deleted');

            $msg = 'New Sales Person '.$salesperson_name.' Has Been Assigned Against Quotation #'.$quotation->quote_no.' by '.auth()->user()->full_name;
            $url = '/viewPriceComparison/'.$quotation->id;
            ActivityLog::createLog(auth()->user(),$msg,'price-comparison',$url,$quotation->id,'updated');

            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Sales Person Assigned Successfully', 'data' => $data]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }



}
