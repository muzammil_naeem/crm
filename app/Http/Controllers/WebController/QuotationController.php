<?php

namespace App\Http\Controllers\WebController;

use App\ActivityLog;
use App\Http\Requests\CustomerFormValidation;
use App\Quotation;
use App\User;
use Illuminate\Routing\Controller;

class QuotationController extends Controller
{
    public function index()
    {
        try
        {

            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Success', 'data' => new \stdClass()]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function create()
    {
        try
        {

            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Create Data Sent', 'data' => new \stdClass()]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function store(CustomerFormValidation $request)
    {
        try
        {
            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Customer Created Successfully', 'data' => new \stdClass()]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function show($id)
    {
        try
        {

            return response()->json(['code' => 200, 'status' => 'success' ,'message' => '', 'data' => new \stdClass()]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function edit($id)
    {
        try
        {

            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Create Data Sent', 'data' => new \stdClass()]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function update(CustomerFormValidation $request, $id)
    {
        try
        {
            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Data Updated Successfully', 'data' => new \stdClass()]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function destroy($id)
    {
        try
        {

            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Activity Log Deleted Successfully', 'data' => new \stdClass()]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function expiredQuotations()
    {
        try
        {

            $user = auth()->user();
            $role = $user->roles()->first();

            $data['quotations'] = Quotation::with(['customer','salesperson','accepted_by'])->where('status','expired');
            $data['salespersons'] = User::where('user_type','salesperson')->orderBy('id','Desc')->get();

            if(strtolower($role->name) != 'admin')
                $data['quotations'] = $data['quotations']->where('user_id',$user->id);

            $data['quotations'] = $data['quotations']->orderBy('id','desc')->get();

            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Success', 'data' => $data]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

}
