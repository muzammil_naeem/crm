<?php

namespace App\Http\Controllers\WebController;

use App\ActivityLog;
use App\Http\Requests\CustomerFormValidation;
use App\Http\Requests\PartsFormValidation;
use App\Http\Requests\QuotationRejectionQuestionValidation;
use App\Jobs\SendCustomerInvoiceAcceptanceEmail;
use App\Jobs\SendCustomerInvoiceRejectanceEmail;
use App\Part;
use App\Quotation;
use App\QuotationRejectionAnswers;
use App\QuotationRejectionQuestion;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Session;

class QuotationRejectionQuestions extends Controller
{
    public function index()
    {
        try
        {
            $data['questions'] = QuotationRejectionQuestion::where('is_deleted',0)->orderBy('id','desc')->get();

            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Success', 'data' =>  $data]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function create()
    {
        try
        {
            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Create Data Sent', 'data' =>  new \stdClass()]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function store(QuotationRejectionQuestionValidation $request)
    {
        try
        {
            $part = new QuotationRejectionQuestion();
            $part->question = $request->question;
            $part->save();

            $msg = 'New Question Has been Created by '.auth()->user()->full_name;
            $url = '';
            ActivityLog::createLog(auth()->user(),$msg,'rejection_questions',$url,$part->id,'created');

            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Question Created Successfully', 'data' => new \stdClass()]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function show($id)
    {
        try
        {


            return response()->json(['code' => 200, 'status' => 'success' ,'message' => '', 'data' => new \stdClass()]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function edit($id)
    {
        try
        {
            $data['question'] = QuotationRejectionQuestion::find($id);

            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Edit Data Sent', 'data' =>  $data]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function update(PartsFormValidation $request, $id)
    {
        try
        {
            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Data Updated Successfully', 'data' => new \stdClass()]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function destroy($id)
    {
        try
        {

            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Parts Deleted Successfully', 'data' => new \stdClass()]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function toggleStatus($id)
    {
        try
        {
            $question = QuotationRejectionQuestion::find($id);
            $question->status = $question->status == 'active' ? 'inactive' : 'active';
            $question->save();

            $data['question'] = $question;

            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Now Quotation Is In '.$question->status.' Successfully', 'data' => $data]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function getQuotaionRejectionForm($id)
    {
        $quotation = Quotation::with(['salesperson','customer'])->find($id);
        $quotation->status = 'rejected';

        $data['quotation'] = $quotation;
        $data['customer'] = $data['quotation']['customer'];
        $data['salesperson'] = $data['quotation']['salesperson'];

        /*SendCustomerInvoiceRejectanceEmail::dispatch($data);*/

        $basepath = storage_path("app/public/invoices/".$data['quotation']['id']."/invoice.pdf");
        \Mail::send('AdminSide.Email.customerRejectanceEmail', ['data' => $data], function ($message) use ($data,$basepath) {
            $message->subject('Customer Invoice Rejection');
            $message->to($data['salesperson']['email']);
            $message->from(config()->get('mail.username'), $data['customer']['full_name']);
            $message->attach($basepath);
        });

        $quotation->save();
        $data['questions'] = QuotationRejectionQuestion::where([['status','active'],['is_deleted',0]])->orderBy('id','desc')->get();

        return view('AdminSide.Form.quotation_rejection_form',compact('data'));
    }

    public function postQuotaionRejectionForm(Request $request,$id)
    {
        $quotation = Quotation::with(['salesperson','customer'])->find($id);

        $answers = $request->answers;
        if($answers)
        {
            foreach ($answers as $key => $answer)
            {
                $quotation_rejection_answers = new QuotationRejectionAnswers();
                $quotation_rejection_answers->quotation_id = $quotation->id;
                $quotation_rejection_answers->quotation_rejection_question_id = $key;
                $quotation_rejection_answers->answer = $answer;
                $quotation_rejection_answers->save();
            }
        }

        Session::flash('message', 'Your Quotation Rejected Successfully !!!');

        return redirect()->route('viewInvoiceCustomer',$quotation->id);
    }
}
