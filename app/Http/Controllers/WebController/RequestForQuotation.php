<?php

namespace App\Http\Controllers\WebController;

use App\ActivityLog;
use App\Http\Requests\CustomerFormValidation;
use App\Http\Requests\PrepareQuotationFormValidation;
use App\Http\Requests\RequestForQuotationFormValidation;
use App\Jobs\ReRequestQuotation;
use App\Part;
use App\Quotation;
use App\User;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Request;
use Session;

class RequestForQuotation extends Controller
{
    public function index()
    {
        try
        {
            $user = auth()->user();
            $role = $user->roles()->first();

            $data['quotations'] = Quotation::with(['customer','salesperson'])->where('status','pending');

            if(strtolower($role->name) != 'admin')
                $data['quotations'] = $data['quotations']->where('user_id',$user->id);

            $data['quotations'] = $data['quotations']->orderBy('id','desc')->get();

            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Success', 'data' => $data]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function create()
    {
        try
        {
            $data['quote_no'] = uniqid();
            $data['salesperson'] = auth()->user();

            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Create Data Sent', 'data' => $data]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function store(RequestForQuotationFormValidation $request)
    {
        try
        {
            $user = auth()->user();

            $quotation = new Quotation();
            $quotation->user_id = $user->id;
            $quotation->customer_id = $request->customer_id;
            $quotation->quote_no = $request->quote_no;
            $quotation->email = $request->customer_email;
            $quotation->contact_no = $request->contact_no;
            $quotation->additional_instructions = $request->additional_instructions;
            $quotation->quotation_expiry = $request->quotation_expiry;

            $attributes = [];
            foreach ($request->part_id as $key => $part)
            {
                $attributes[$part] = [
                    'quantity' => $request->quantity[$key],
                    'condition' => $request->condition[$key],
                    'price' => $request->price[$key],
                    'extended_price' => $request->extended_price[$key],
                ];
            }

            $quotation->save();
            $quotation->parts()->sync($attributes);

            $data['quotation'] = $quotation;

            $msg = 'Reqest For Quotation #'.$quotation->quote_no.' has been Created by '.auth()->user()->full_name;
            $url = '/request_for_quote/view/'.$quotation->id;
            ActivityLog::createLog(auth()->user(),$msg,'request-for-quotation',$url,$quotation->id,'created');

            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Request For Quotation Submitted Successfully', 'data' => $data]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function show($id)
    {
        try
        {

            return response()->json(['code' => 200, 'status' => 'success' ,'message' => '', 'data' => new \stdClass()]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function edit($id)
    {
        try
        {
            $data['quotation'] = Quotation::with(['customer','salesperson','parts'])->find($id);
            $data['salesperson'] = $data['quotation']['salesperson'];
            $part_ids = $data['quotation']['parts']->pluck('id')->toArray();
            $data['parts'] = Part::whereIn('id',$part_ids)->orderBy('id','desc')->get();

            $data['quotation']['parts']->map(function ($part)
            {
                $part['quantity'] = $part['pivot']['quantity'];
                $part['condition'] = $part['pivot']['condition'];
                $part['price'] = $part['pivot']['price'];
                $part['extended_price'] = $part['pivot']['extended_price'];
            });

            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Create Data Sent', 'data' => $data]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function update(RequestForQuotationFormValidation $request, $id)
    {
        try
        {
            $quotation = Quotation::find($id);
            $quotation->customer_id = $request->customer_id;
            $quotation->email = $request->customer_email;
            $quotation->contact_no = $request->contact_no;
            $quotation->additional_instructions = $request->additional_instructions;
            $quotation->quotation_expiry = $request->quotation_expiry;

            $attributes = [];
            foreach ($request->part_id as $key => $part)
            {
                $attributes[$part] = [
                    'quantity' => $request->quantity[$key],
                    'condition' => $request->condition[$key],
                    'price' => $request->price[$key],
                    'extended_price' => $request->extended_price[$key],
                ];
            }

            $quotation->save();
            $quotation->parts()->sync($attributes);

            $msg = 'Reqest For Quotation #'.$quotation->quote_no.' has been Updated by '.auth()->user()->full_name;
            $url = '/request_for_quote/view/'.$quotation->id;
            ActivityLog::createLog(auth()->user(),$msg,'request-for-quotation',$url,$quotation->id,'updated');

            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Request For Quotation Updated Successfully', 'data' => new \stdClass()]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function destroy($id)
    {
        try
        {
            $quotation = Quotation::find($id);
            $quotation_obj = $quotation;
            $quotation->delete();

            $msg = 'Request For Quotation #'.$quotation_obj->quote_no.' Deleted by '.auth()->user()->full_name;
            ActivityLog::createLog(auth()->user(),$msg,'request-for-quotation',null,$quotation->id,'deleted');

            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Quotation Deleted Successfully', 'data' => new \stdClass()]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function reRequestForQuotation($id)
    {
        try
        {
            $quotation = Quotation::with(['customer','salesperson'])->find($id);

            $data['quotation'] = $quotation;
            $data['customer'] = $data['quotation']['customer'];
            $data['salesperson'] = $data['quotation']['salesperson'];

            /*ReRequestQuotation::dispatch($data);*/

            \Mail::send('AdminSide.Email.reRequestQuotationEmail', ['data' => $data], function ($message) use ($data) {
                $message->subject('Request For Quotation');
                $message->to($data['salesperson']['email']);
                $message->from(config()->get('mail.username'), $data['customer']['full_name']);
            });

            $msg = 'Customer '.$quotation['customer']->full_name.' Request For Againt Quotation #'.$quotation->quote_no.' To '.$quotation['salesperson']->full_name;
            ActivityLog::createLog($quotation['salesperson'],$msg,'request-for-quotation',null,$quotation->id,'updated');

            Session::flash('message', 'Request For Quotation Sent Successfully !!!');

            return redirect()->back();

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

}
