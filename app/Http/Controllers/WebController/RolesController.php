<?php

namespace App\Http\Controllers\WebController;

use App\ActivityLog;
use App\Http\Requests\CreateRoleValidation;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolesController extends Controller
{
    public function index()
    {
        try
        {
            $data['roles'] = Role::orderBy('id','desc')->get();
            $data['roles']->map(function ($q){
                $q->created_at = date('Y-m-d', strtotime($q->created_at));
            });

            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Company Created Successfully', 'data' => $data]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function create()
    {
        try
        {
            $permissions = config('user-permissions');
            $data['permissions'] = $permissions;

            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Create Data Sent', 'data' => $data]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function store(CreateRoleValidation $request)
    {
        try
        {
            $checked_permissions = $request->permissions;
            $role = Role::create(['name' => $request->role_name]);
            if (!empty($checked_permissions)) {
                foreach ($checked_permissions as $checked_permission){
                    Permission::findOrCreate($checked_permission);
                }
                $role->givePermissionTo($checked_permissions);
            }

            $msg = 'New '. $role->name .' Role has been Created by '.auth()->user()->full_name;
            $url = '/roles/edit/'.$role->id;
            ActivityLog::createLog(auth()->user(),$msg,'role',$url,$role->id,'created');

            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Role Created Successfully', 'data' => new \stdClass()]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function show($id)
    {
        try
        {


            return response()->json(['code' => 200, 'status' => 'success' ,'message' => '', 'data' => new \stdClass()]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function edit($id)
    {
        try
        {
            $role = Role::findById($id);
            $role_permissions = array_column($role->permissions()->get()->toArray(), 'name');

            $permissions = config('user-permissions');
            $data['permissions'] = $permissions;
            $data['role_permissions'] = $role_permissions;
            $data['role'] = $role;

            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Edit Data Sent', 'data' => $data]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function update(CreateRoleValidation $request, $id)
    {
        try
        {
            $checked_permissions = $request->permissions;
            $role = Role::findById($id);
            if (!empty($checked_permissions)) {
                foreach ($checked_permissions as $checked_permission){
                    Permission::findOrCreate($checked_permission);
                }
                $role->syncPermissions($checked_permissions);
            }

            $msg = $role->name .' Role has been Updated by '.auth()->user()->full_name;
            $url = '/roles/edit/'.$role->id;
            ActivityLog::createLog(auth()->user(),$msg,'role',$url,$role->id,'updated');

            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Data Updated Successfully', 'data' => new \stdClass()]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function destroy($id)
    {
        try
        {
            //if role assign to any user then dont delete role.
            $role = Role::findById($id);
            if($role->users->isNotEmpty())
                return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => 'You Cannot Delete As User Assigned To This Role.','data' => new \stdClass()]);

            $role_obj = $role;
            $role->revokePermissionTo($role->permissions()->get());
            $role->delete();

            $msg = $role_obj->name.' Role has been Deleted by '.auth()->user()->full_name;
            ActivityLog::createLog(auth()->user(),$msg,'role',null,$role_obj->id,'deleted');

            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Company Deleted Successfully', 'data' => new \stdClass()]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

}
