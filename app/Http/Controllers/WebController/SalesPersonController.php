<?php

namespace App\Http\Controllers\WebController;

use App\ActivityLog;
use App\Http\Requests\CreateRoleValidation;
use App\Http\Requests\SalesPersonFormValidation;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Crypt;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class SalesPersonController extends Controller
{
    public function index()
    {
        try
        {
            $data['salespersons'] = User::where([['user_type','salesperson'],['id','!=',auth()->user()->id]])->orderBy('id','desc')->get();
            $data['salespersons']->map(function ($q)
            {
                $q['role'] = $q->roles()->first();
            });

            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Success', 'data' => $data]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function create()
    {
        try
        {
            $data['roles'] = Role::all();

            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Create Data Sent', 'data' => $data]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function store(SalesPersonFormValidation $request)
    {
        try
        {
            $role = Role::findById($request->role_id);
            $user = User::create([
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'email' => $request->email,
                'user_type' => 'salesperson',
                'password' => bcrypt($request->password),
                'host' => $request->host,
                'email_username' => $request->email_username,
                'email_password' => Crypt::encryptString($request->email_password),
            ]);

            $user->assignRole($role);

            $msg = 'New Sales Person '. $user->full_name .' has been Created by '.auth()->user()->full_name;
            $url = '/sales_person/edit/'.$user->id;
            ActivityLog::createLog(auth()->user(),$msg,'salesperson',$url,$user->id,'created');

            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Sales Person Created Successfully', 'data' => new \stdClass()]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function show($id)
    {
        try
        {


            return response()->json(['code' => 200, 'status' => 'success' ,'message' => '', 'data' => new \stdClass()]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function edit($id)
    {
        try
        {
            $auth_user = auth()->user();
            $data['roles'] = Role::all();
            $user = User::find($id);
            $data['sales_person'] = $user;
            $data['sales_person_role'] = $user->roles()->first();
            $data['login_user_role'] = $auth_user->roles()->first();
            $data['login_user_id'] = auth()->user()->id;

            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Create Data Sent', 'data' => $data]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function update(SalesPersonFormValidation $request, $id)
    {
        try
        {
            $role = Role::findById($request->role_id);
            $user = User::find($id);
            $user->first_name = $request->first_name;
            $user->last_name = $request->last_name;
            $user->email = $request->email;

            if(isset($request->host) && !empty($request->host))
                $user->host = $request->host;
            if(isset($request->email_username) && !empty($request->email_username))
                $user->email_username = $request->email_username;
            if(isset($request->email_password) && !empty($request->email_password))
                $user->email_password = Crypt::encryptString($request->email_password);
            if(!empty($request->password))
                $user->password = $request->password;

            $user->save();
            $user->syncRoles($role->name);

            $msg = 'Sales Person '. $user->full_name .' has been Updated by '.auth()->user()->full_name;
            $url = '/sales_person/edit/'.$user->id;
            ActivityLog::createLog(auth()->user(),$msg,'salesperson',$url,$user->id,'updated');

            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Data Updated Successfully', 'data' => new \stdClass()]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

    public function destroy($id)
    {
        try
        {
            $user = User::find($id);
            $user_obj = $user;
            $user->delete();

            $msg = 'Sales Person '.$user_obj->full_name.' Deleted by '.auth()->user()->full_name;
            ActivityLog::createLog(auth()->user(),$msg,'salesperson',null,$user->id,'deleted');

            return response()->json(['code' => 200, 'status' => 'success' ,'message' => 'Sales Person Deleted Successfully', 'data' => new \stdClass()]);

        } catch (\Exception $ex) {
            return response()->json(['code' => 422 , 'status' => 'failure' ,'message' => $ex->getMessage(),'data' => new \stdClass()]);
        }
    }

}
