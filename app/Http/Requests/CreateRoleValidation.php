<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateRoleValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'role_name' => 'required',
            'permissions' => 'required',
        ];
    }

    public function messages() {
        return [
            'permissions.required' => "Please Select Permissions",
            'role_name.required' => "Please Enter Role Name",
        ];
    }
}
