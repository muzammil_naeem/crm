<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CustomerFormValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('customer') ? $this->route('customer') :0;

        $rules=[
            'first_name' => 'required',
            'last_name' => 'required',
            'contact_no' => 'required|numeric|min:7',
            'company_name' => 'required',
            'address' => 'required',
            'customer_no' => 'required',
            'source' => 'required',
            'platform' => 'required_if:source,other-platform',
        ];

        if($id){
            $rules['email']='required|unique:users,email,'.$id;
        }
        else{
            $rules['email']='required|email|unique:users,email';
        }

        return $rules;
    }

    public function messages() {
        return [
            'first_name.required' => "Please Enter First Name",
            'last_name.required' => "Please Enter Last Name",
            'email.required' => "Please Enter Email",
            'contact_no.required' => "Please Contact Number",
            'company_name.required' => "Please Copmany Name",
            'address.required' => "Please Customer Address",
            'source.required' => "Please Select Source",
        ];
    }
}
