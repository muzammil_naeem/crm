<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PartsFormValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('part') ? $this->route('part') :0;

        $rules['part_no'] = 'required';
        $rules['name'] = 'required';

        if($id){
            $rules['part_no']='required|unique:parts,part_no,'.$id;
        }
        else{
            $rules['part_no']='required|unique:parts,part_no';
        }

        return $rules;
    }

    public function messages() {
        return [
            'name.required' => "Please Enter Part Name",
            'part_no.required' => "Please Enter Part Number",
            'part_no.unique' => "Part Number has already been taken",
        ];
    }
}
