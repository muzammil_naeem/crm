<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PrepareQuotationFormValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules=[
            'fst_cheapest_price.*' => 'required',
            'fst_company_name.*' => 'required',
            'fst_product_url.*' => 'required',
            'fst_stock.*' => 'required',
            'fst_lead_time_type.*' => 'required',
            'fst_lead_time_value.*' => 'required',
            'second_cheapest_price.*' => 'required',
            'second_company_name.*' => 'required',
            'second_product_url.*' => 'required',
            'second_stock.*' => 'required',
            'second_lead_time_type.*' => 'required',
            'second_lead_time_value.*' => 'required',
            'third_cheapest_price.*' => 'required',
            'third_company_name.*' => 'required',
            'third_product_url.*' => 'required',
            'third_stock.*' => 'required',
            'third_lead_time_type.*' => 'required',
            'third_lead_time_value.*' => 'required',
            'quoted_price.*' => 'required',
            'source_price.*' => 'required',
            'source_company.*' => 'required',
        ];

        return $rules;
    }

    public function messages() {
        return [

        ];
    }
}