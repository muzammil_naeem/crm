<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequestForQuotationFormValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules=[
            'customer_id' => 'required',
            'customer_email' => 'required|email',
            'contact_no' => 'required|numeric|min:7',
            'quotation_expiry' => 'required|after:today',
        ];

        return $rules;
    }

    public function messages() {
        return [
            'customer_id.required' => "Please Select Customer ID",
            'customer_email.required' => "Please Enter Email",
            'contact_no.required' => "Please Enter Contact Number",
        ];
    }
}