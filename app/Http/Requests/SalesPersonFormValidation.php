<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SalesPersonFormValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('salesperson') ? $this->route('salesperson') :0;

        $user = auth()->user();
        $login_user_id = $user->id;
        $role = $user->roles()->first();
        $updating_user_id = $this->id;

        $rules=[
			'role_id' => 'required|exists:roles,id',
			'first_name' => 'required',
			'last_name' => 'required',
        ];

        if($id){
            $rules['password']= 'nullable|min:6';
            $rules['email']='required|unique:users,email,'.$id;

            if($login_user_id == $updating_user_id || $role->name == 'admin')
            {
                $rules['host'] = 'required';
                $rules['email_username'] = 'required|email';
                $rules['email_password'] = 'required|min:6';
            }
        }
        else{
            $rules['host'] = 'required';
			$rules['email_username'] = 'required|email';
			$rules['email_password'] = 'required|min:6';
            $rules['password']= 'required|min:6';
            $rules['email']='required|email|unique:users,email';
        }

        return $rules;
    }

    public function messages() {
        return [
            'role_id.required' => "Please Select the Role",
            'first_name.required' => "Please Enter First Name",
            'last_name.required' => "Please Enter Last Name",
            'email.required' => "Please Enter Email",
            'password.required' => "Please Enter Password",
        ];
    }
}
