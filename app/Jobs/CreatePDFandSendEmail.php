<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use PDF;
use File;
use Mail;

class CreatePDFandSendEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data = $this->data;

        $pdf = PDF::loadView('AdminSide.Invoice.downloadInvoice', compact('data'))->setPaper(array(0,0,780,1440),'portrait');
        $basepath = storage_path("app/public/invoices/".$data['quotation']['id']);
        if (!file_exists($basepath)) {
            File::makeDirectory($basepath, 0777, true);
        }
        $pdf->save($basepath.'/invoice.pdf');

        /*Mail::send('AdminSide.Email.generateInvoiceEmail', ['data' => $data], function ($message) use ($data) {
            $message->subject('Customer Invoice');
            $message->to($data['quotation']['email']);
            $message->from($data['salesperson']['email'], $data['salesperson']['full_name']);
        });*/
    }
}
