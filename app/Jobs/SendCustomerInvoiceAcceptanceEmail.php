<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Mail;

class SendCustomerInvoiceAcceptanceEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $data;
    protected $basepath;

    public function __construct($data)
    {
        $this->data = $data;
        $this->basepath = '';
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data = $this->data;
        $this->basepath = storage_path("app/public/invoices/".$data['quotation']['id']."/invoice.pdf");
        Mail::send('AdminSide.Email.customerAcceptanceEmail', ['data' => $data], function ($message) use ($data) {
            $message->subject('Customer Invoice Acceptance');
            $message->to($data['salesperson']['email']);
            $message->from(config()->get('mail.username'), $data['customer']['full_name']);
            $message->attach($this->basepath);
        });
    }
}
