<?php

namespace App\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        Blade::component('AdminSide.PartialViews.RoundAlerts.danger', 'roundalert_danger');
        Blade::component('AdminSide.PartialViews.RoundAlerts.success', 'roundalert_success');

        Blade::component('AdminSide.PartialViews.Datatable.datatable', 'datatable');
        Blade::component('AdminSide.PartialViews.Datatable.datatablewithoutcard', 'datatablewithoutcard');

        Blade::component('AdminSide.PartialViews.Card.card', 'card');

        Blade::component('AdminSide.PartialViews.notestitletext', 'notestitletext');

        //Modals Start
            Blade::component('AdminSide.PartialViews.Modal.successmodal', 'successmodal');
            Blade::component('AdminSide.PartialViews.Modal.dangermodal', 'dangermodal');
            Blade::component('AdminSide.PartialViews.Modal.primarymodal', 'primarymodal');
            Blade::component('AdminSide.PartialViews.Modal.infomodal', 'infomodal');
            Blade::component('AdminSide.PartialViews.Modal.warningmodal', 'warningmodal');
            Blade::component('AdminSide.PartialViews.Modal.bordermodal', 'bordermodal');
        //Modal End

        //Small Coloured Card Start
            Blade::component('AdminSide.PartialViews.SmallCard.smallcolouredcard', 'smallcolouredcard');
        //Small Coloured Card End

        //Sales Person
        Blade::include('AdminSide.SalesPerson.PartialView.salespersonform', 'salespersonform');

        //Customer
        Blade::include('AdminSide.Customer.PartialView.customerform', 'customerform');

        //Ownership
        Blade::include('AdminSide.Ownership.PartialView.ownershipform', 'ownerhsipform');

        //Role
        Blade::include('AdminSide.Role.PartialView.createroleform', 'createroleform');

        //Operation Location
        Blade::include('AdminSide.OperationLocation.PartialView.createcitiesform', 'createcitiesform');

        //Permission
        Blade::include('AdminSide.Permission.PartialView.createpermissionform', 'createpermissionform');

        //Leads
        Blade::include('AdminSide.Leads.PartialViews.createleadform', 'createleadform');

        //Deals
        Blade::include('AdminSide.Deals.PartialViews.createdealform', 'createdealform');

        //Account
        Blade::include('AdminSide.Accounts.PartialView.createaccountform', 'createaccountform');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
