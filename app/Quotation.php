<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Quotation extends Model
{
    use SoftDeletes;
    protected $fillable = ['status'];

    public function getCreatedAtAttribute($value)
    {
        return date('Y-m-d', strtotime($value));
    }

    public function customer()
    {
        return $this->belongsTo(User::class,'customer_id','id')->where('user_type','customer');
    }

    public function salesperson()
    {
        return $this->belongsTo(User::class,'user_id','id')->where('user_type','salesperson');
    }

    public function accepted_by()
    {
        return $this->belongsTo(User::class,'accepted_by','id');
    }

    public function parts(){
        return $this->belongsToMany(Part::class,
            'quotation_parts', 'quotation_id', 'part_id')
            ->withPivot(
                'quantity','condition','price','extended_price','grand_extended_price'
                ,'fst_cheapest_price'
                ,'fst_company_name'
                ,'fst_product_url'
                ,'fst_stock'
                ,'fst_lead_week'
                ,'fst_lead_day'
                ,'fst_additional_info'
                ,'second_cheapest_price'
                ,'second_company_name'
                ,'second_product_url'
                ,'second_stock'
                ,'second_lead_week'
                ,'second_lead_day'
                ,'second_additional_info'
                ,'third_cheapest_price'
                ,'third_company_name'
                ,'third_product_url'
                ,'third_stock'
                ,'third_lead_week'
                ,'third_lead_day'
                ,'third_additional_info'
                ,'quoted_price'
                ,'source_price'
                ,'source_company'
                ,'custom_note'
                ,'fst_lead_time_type'
                ,'fst_lead_time_value'
                ,'second_lead_time_type'
                ,'second_lead_time_value'
                ,'third_lead_time_type'
                ,'third_lead_time_value'
        );
    }

    public function followups(){
        return $this->belongsToMany(User::class, 'quotation_followups', 'quotation_id', 'followup_by')->withPivot(
            'comments',
            'followup_call',
            'followup_email'
        )->withTimestamps();
    }

    public function rejection_answers(){
        return $this->hasMany(QuotationRejectionAnswers::class,  'quotation_id', 'id');
    }
}
