<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuotationRejectionAnswers extends Model
{
    public function getCreatedAtAttribute($value)
    {
        return date('Y-m-d', strtotime($value));
    }

    public function question()
    {
        return $this->belongsTo(QuotationRejectionQuestion::class,'quotation_rejection_question_id','id');
    }
}
