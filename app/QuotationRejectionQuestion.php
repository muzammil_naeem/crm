<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuotationRejectionQuestion extends Model
{
    public function getCreatedAtAttribute($value)
    {
        return date('Y-m-d', strtotime($value));
    }
}
