<?php
return [
    'customer' => [
        'list',
        'create',
        'view',
        'edit',
        'delete',
    ],
    'roles' => [
        'list',
        'create',
        'view',
        'edit',
        'delete',
    ],
    'request_for_quotation' => [
        'list',
        'view',
        'create',
        'edit',
        'delete',
    ],
    'price_comparison' => [
        'list',
        'view',
        'create',
        'edit',
        'delete',
    ],
    'invoice' => [
        'list',
        'view',
        'create',
        'upload_purchase_order',
        'complete_quotation',
    ],
    'review_quotation' => [
        'edit',
    ],
    'assign_sales_person' => [
        'edit',
    ],
    'follow_up' => [
        'pending-follow-ups',
        'create',
        'list',
    ],
    'expired_quotations' => [
        'list',
    ],
    'find_quotation_by_id' => [
        'view',
    ],
    'accounts' => [
        'list',
    ],
    'leads' => [
        'list',
    ],
    'rejection_questions' => [
        'list',
        'create',
        'update',
    ],
    'sales_person' => [
        'list',
        'create',
        'view',
        'edit',
        'delete',
        'invoice-acceptance',
    ],
    'parts' => [
        'list',
        'create',
        'view',
        'edit',
        'delete',
    ],
    'activity_log' => [
        'list',
        'delete',
    ],
];