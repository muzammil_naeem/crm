<?php

use Faker\Generator as Faker;

$factory->define(App\Part::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'part_no' => $faker->unique()->uuid,
    ];
});
