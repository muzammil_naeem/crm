<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCompanyNameAddressInUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer('parent_id')->default(0)->after('id');
            $table->string('source')->after('email')->nullable();
            $table->text('address')->after('email')->nullable();
            $table->string('company_name')->after('email')->nullable();
            $table->string('contact_no')->after('email')->nullable();
            $table->enum('user_type', array('salesperson', 'customer'))->after('email')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('parent_id');
            $table->dropColumn('user_type');
            $table->dropColumn('contact_no');
            $table->dropColumn('company_name');
            $table->dropColumn('address');
            $table->dropColumn('source');
        });
    }
}
