<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuotationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotations', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('customer_id');
            $table->bigInteger('user_id');
            $table->string('quote_no')->nullable();
            $table->enum('status', array("pending","quotation-prepared","invoice-sent","completed"))->default('pending');
            $table->string('email')->nullable();
            $table->string('contact_no')->nullable();
            $table->bigInteger('accepted_by')->nullable();
            $table->text('additional_instructions')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quotations');
    }
}
