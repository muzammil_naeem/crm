<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuotationPartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotation_parts', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('quotation_id');
            $table->bigInteger('part_id');
            $table->integer('quantity')->default(0);
            $table->enum('condition', array('New', 'Used','Refurbished','Any'))->nullable();
            $table->double('price')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quotation_parts');
    }
}
