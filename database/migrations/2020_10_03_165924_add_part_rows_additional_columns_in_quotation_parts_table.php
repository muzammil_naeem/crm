<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPartRowsAdditionalColumnsInQuotationPartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('quotation_parts', function (Blueprint $table) {
            $table->double('fst_cheapest_price')->nullable();
            $table->string('fst_company_name')->nullable();
            $table->string('fst_product_url')->nullable();
            $table->enum('fst_stock',array('Instock','Outstock'))->default('Instock');
            $table->integer('fst_lead_week')->default(0);
            $table->integer('fst_lead_day')->default(0);
            $table->text('fst_additional_info')->nullable();

            $table->double('second_cheapest_price')->nullable();
            $table->string('second_company_name')->nullable();
            $table->string('second_product_url')->nullable();
            $table->enum('second_stock',array('Instock','Outstock'))->default('Instock');
            $table->integer('second_lead_week')->default(0);
            $table->integer('second_lead_day')->default(0);
            $table->text('second_additional_info')->nullable();

            $table->double('third_cheapest_price')->nullable();
            $table->string('third_company_name')->nullable();
            $table->string('third_product_url')->nullable();
            $table->enum('third_stock',array('Instock','Outstock'))->default('Instock');
            $table->integer('third_lead_week')->default(0);
            $table->integer('third_lead_day')->default(0);
            $table->text('third_additional_info')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('quotation_parts', function (Blueprint $table) {
            $table->dropColumn('fst_cheapest_price');
            $table->dropColumn('fst_company_name');
            $table->dropColumn('fst_product_url');
            $table->dropColumn('fst_stock');
            $table->dropColumn('fst_lead_week');
            $table->dropColumn('fst_lead_day');
            $table->dropColumn('fst_additional_info');

            $table->dropColumn('second_cheapest_price');
            $table->dropColumn('second_company_name');
            $table->dropColumn('second_product_url');
            $table->dropColumn('second_stock');
            $table->dropColumn('second_lead_week');
            $table->dropColumn('second_lead_day');
            $table->dropColumn('second_additional_info');

            $table->dropColumn('third_cheapest_price');
            $table->dropColumn('third_company_name');
            $table->dropColumn('third_product_url');
            $table->dropColumn('third_stock');
            $table->dropColumn('third_lead_week');
            $table->dropColumn('third_lead_day');
            $table->dropColumn('third_additional_info');
        });
    }
}
