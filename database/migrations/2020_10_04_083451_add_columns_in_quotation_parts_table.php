<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsInQuotationPartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('quotation_parts', function (Blueprint $table) {
            $table->double('quoted_price')->nullable();
            $table->double('source_price')->nullable();
            $table->string('source_company')->nullable();
            $table->text('custom_note')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('quotation_parts', function (Blueprint $table) {
            $table->dropColumn('quoted_price');
            $table->dropColumn('source_price');
            $table->dropColumn('source_company');
            $table->dropColumn('custom_note');
        });
    }
}
