<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddShipmentCostColumnsInQuotationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('quotations', function (Blueprint $table) {
            $table->double('grand_total')->default(0)->after('contact_no');
            $table->double('shipment_cost')->default(0)->after('contact_no');
            $table->double('total')->default(0)->after('contact_no');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('quotations', function (Blueprint $table) {
            $table->dropColumn('total');
            $table->dropColumn('shipment_cost');
            $table->dropColumn('grand_total');
        });
    }
}
