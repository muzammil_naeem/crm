<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class ChangeFieldTypeFromEnumToStringInQuotationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE `quotations` CHANGE `status` `status` ENUM('pending', 'quotation-prepared', 'invoice-sent', 'completed','rejected')  NOT NULL DEFAULT 'pending'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE `quotations` CHANGE `status` `status` ENUM('pending', 'quotation-prepared', 'invoice-sent', 'completed')  NOT NULL DEFAULT 'pending'");
    }
}
