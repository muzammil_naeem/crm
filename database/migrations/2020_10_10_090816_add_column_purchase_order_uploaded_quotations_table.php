<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnPurchaseOrderUploadedQuotationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('quotations', function (Blueprint $table) {
            $table->text('comments')->after('accepted_by')->nullable();
            $table->integer('purchase_order_uploaded')->after('accepted_by')->default(0);
            $table->string('purchase_order_file_name')->after('accepted_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('quotations', function (Blueprint $table) {
            $table->dropColumn('purchase_order_uploaded');
            $table->dropColumn('comments');
            $table->dropColumn('purchase_order_file_name');
        });
    }
}
