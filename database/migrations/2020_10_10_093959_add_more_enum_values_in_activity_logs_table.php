<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMoreEnumValuesInActivityLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE `activity_logs` CHANGE `action` `action` ENUM('created', 'updated', 'deleted', 'assigned','uploaded','accepted','completed')");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE `activity_logs` CHANGE `action` `action` ENUM('created', 'updated', 'deleted', 'assigned')");
    }
}
