<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLeadTimeTypeAndValueInQuotationPartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('quotation_parts', function (Blueprint $table) {
            $table->integer('fst_lead_time_value')->after('fst_stock')->default(0);
            $table->enum('fst_lead_time_type',['day','week','month'])->after('fst_stock')->nullable();

            $table->integer('second_lead_time_value')->after('second_stock')->default(0);
            $table->enum('second_lead_time_type',['day','week','month'])->after('second_stock')->nullable();

            $table->integer('third_lead_time_value')->after('third_stock')->default(0);
            $table->enum('third_lead_time_type',['day','week','month'])->after('third_stock')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('quotation_parts', function (Blueprint $table) {
            $table->dropColumn('fst_lead_time_type');
            $table->dropColumn('fst_lead_time_value');
            $table->dropColumn('second_lead_time_type');
            $table->dropColumn('second_lead_time_value');
            $table->dropColumn('third_lead_time_type');
            $table->dropColumn('third_lead_time_value');
        });
    }
}
