<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExtendedPriceColumnInQuotationPartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('quotation_parts', function (Blueprint $table) {
            $table->double('grand_extended_price')->after('price')->default(0);
            $table->double('extended_price')->after('price')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('quotation_parts', function (Blueprint $table) {
            $table->dropColumn('extended_price');
            $table->dropColumn('grand_extended_price');
        });
    }
}
