<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnFollowupThroughInQuotationFollowupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('quotation_followups', function (Blueprint $table) {
            $table->boolean('followup_email')->after('comments')->default(0);
            $table->boolean('followup_call')->after('comments')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('quotation_followups', function (Blueprint $table) {
            $table->dropColumn('followup_email');
            $table->dropColumn('followup_call');
        });
    }
}
