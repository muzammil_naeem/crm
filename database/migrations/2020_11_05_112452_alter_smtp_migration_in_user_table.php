<?php

use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSmtpMigrationInUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('email_password');
            $table->dropColumn('email_username');
            $table->dropColumn('encryption');
            $table->dropColumn('port');
            $table->dropColumn('host');
        });

        Schema::table('users', function (Blueprint $table) {
            $encrypted = Crypt::encryptString('P5uqyYVkf5Pc');
            $table->text('email_password')->after('platform')->default($encrypted);
            $table->string('email_username')->after('platform')->default('_mainaccount@hitechhub.tech');
            $table->string('encryption')->after('platform')->default('ssl');
            $table->integer('port')->after('platform')->default('465');
            $table->string('host')->after('platform')->default('hitechhub.tech');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
