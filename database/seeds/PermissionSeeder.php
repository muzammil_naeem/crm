<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function __construct()
    {

    }

    public function run()
    {
        Model::unguard();
        $permissions = config('user-permissions');
        if ($permissions) {
            foreach ($permissions as $key => $permission) {

                foreach ($permission as  $perm) {
                    Permission::findOrCreate( $key . '-' . $perm);
                }
            }

        }
        $role = Role::findOrCreate('admin');
        $role->givePermissionTo(Permission::all());
    }
}
