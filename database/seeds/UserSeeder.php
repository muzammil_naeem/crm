<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Crypt;
use Spatie\Permission\Models\Role;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = Role::findById(1);
        $user = User::create([
            'first_name' => 'Admin',
            'last_name' => 'Person',
            'email' => 'no-reply@servertechsupply.com',
            'user_type' => 'salesperson',
            'password' => bcrypt('123456'),
            'host' => 'servertechsupply.com',
            'email_username' => 'no-reply@servertechsupply.com',
            'email_password' => Crypt::encryptString('123456'),
        ]);

        $user->assignRole($role);
    }
}
