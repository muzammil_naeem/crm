
require('./bootstrap');
window.Vue = require('vue');

import VueRouter from 'vue-router';
window.Vue.use(VueRouter);

import { routes } from './routes/userRoutes';

const router = new VueRouter({
    routes
})

//for spatie permissions start
Vue.directive('can', function (el, binding, vnode) {
    if(Permissions.indexOf(binding.value) !== -1){
        return vnode.elm.hidden = false;
    }else{
        return vnode.elm.hidden = true;
    }
});
//for spatie permissions end

Vue.config.devtools = true
const app = new Vue({
    el: '#app-vue',
    router,
});