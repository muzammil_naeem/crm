export const globalMixin = {
    created() {
    },
    data() {
        return {

        }
    },
    methods: {
        successToastr(title,msg)
        {
            toastr.options = {"closeButton": true, "progressBar": true,};
            toastr.success(msg, title, {"progressBar": true});
        },
        dangerToastr(title,msg)
        {
            toastr.options = {"closeButton": true, "progressBar": true,};
            toastr.error(msg, title, {"progressBar": true});
        },
        warningToastr(title,msg)
        {
            toastr.options = {"closeButton": true, "progressBar": true,};
            toastr.warning(msg, title, {"progressBar": true});
        },
        infoToastr(title,msg)
        {
            toastr.options = {"closeButton": true, "progressBar": true,};
            toastr.info(msg, title, {"progressBar": true});
        },
        primaryToastr(title,msg)
        {
            toastr.options = {"closeButton": true, "progressBar": true,};
            toastr.primary(msg, title, {"progressBar": true});
        },
        initializeDatatable() {
            $('table').DataTable();
        },
        initializeSingleSelect2() {
            $(".select2").select2();
        },
        initializeToolTip() {
            $('.tooltip-vue').tooltip();
        },
        initializeCheckBox() {
            $('input[type="checkbox"], input[type="radio"]').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue'
            });
        }
    }
}