import CreateCustomer from '../components/AdminSide/Customer/createCustomer';
import EditCustomer from '../components/AdminSide/Customer/editCustomer';
import ListCustomer from '../components/AdminSide/Customer/listCustomers';

import CreateSalesPerson from '../components/AdminSide/SalesPerson/createSalesPerson';
import EditSalesPerson from '../components/AdminSide/SalesPerson/editSalesPerson';
import ListSalesPerson from '../components/AdminSide/SalesPerson/listSalesPerson';

import CreateParts from '../components/AdminSide/Parts/createPart';
import EditParts from '../components/AdminSide/Parts/editPart';
import ListParts from '../components/AdminSide/Parts/listParts';

import CreateRoles from '../components/AdminSide/Roles/createRole';
import EditRoles from '../components/AdminSide/Roles/editRole';
import ListRoles from '../components/AdminSide/Roles/listRoles';

import ActivityLog from '../components/AdminSide/ActivityLog/listActivityLogs';

import RequestForQuotation from '../components/AdminSide/Quotation/requestForQuotation';
import EditRequestForQuotation from '../components/AdminSide/Quotation/editRequestForQuotation';
import ViewRequestForQuotation from '../components/AdminSide/Quotation/viewRequestForQuotation';
import PendingQuotation from '../components/AdminSide/Quotation/pendingQuotations';
import FindQuotationById from '../components/AdminSide/Quotation/findQuotationById';

import PriceComparison from '../components/AdminSide/Quotation/PriceComparison/priceComparison';
import CreatePriceComparison from '../components/AdminSide/Quotation/PriceComparison/createPriceComparison';
import EditPriceComparison from '../components/AdminSide/Quotation/PriceComparison/editPriceComparison';
import ViewPriceComparison from '../components/AdminSide/Quotation/PriceComparison/viewPriceComparison';

import ReviewQuotation from '../components/AdminSide/Quotation/ReviewQuotation/reviewQuotation';

import Invoice from '../components/AdminSide/Invoice/invoice';
import ListInvoices from '../components/AdminSide/Invoice/listInvoices';
import FollowUpListing from '../components/AdminSide/Invoice/followUpListing';

import Accounts from '../components/AdminSide/Accounts/accounts.vue';

import ExpiredQuotations from '../components/AdminSide/Quotation/Expired/expiredQuotations';

import PendingFollowups from '../components/AdminSide/Invoice/pendingFollowUps';

import Leads from '../components/AdminSide/Lead/leads';

import Dashboard from '../components/AdminSide/Dashboard/dashboard.vue';

import ListQuotationRejectionQuestion from '../components/AdminSide/Quotation/RejectionQuestion/listQuestions';
import CreateQuotationRejectionQuestion from '../components/AdminSide/Quotation/RejectionQuestion/createQuotationRejectionQuestion';

export const routes = [
    {path: '/', component: Dashboard, name: 'dashboard'},

    {path: '/customers', component: ListCustomer, name: 'listCustomers'},
    {path: '/customers/create', component: CreateCustomer, name: 'createCustomer'},
    {path: '/customers/edit/:id', component: EditCustomer, name: 'editCustomer'},

    {path: '/sales_person', component: ListSalesPerson, name: 'listSalesPerson'},
    {path: '/sales_person/create', component: CreateSalesPerson, name: 'createSalesPerson'},
    {path: '/sales_person/edit/:id', component: EditSalesPerson, name: 'editSalesPerson'},

    {path: '/parts', component: ListParts, name: 'listParts'},
    {path: '/parts/create', component: CreateParts, name: 'createPart'},
    {path: '/parts/edit/:id', component: EditParts, name: 'editPart'},

    {path: '/quotationRejectionQuestions', component: ListQuotationRejectionQuestion, name: 'listQuotationRejectionQuestion'},
    {path: '/quotationRejectionQuestions/create', component: CreateQuotationRejectionQuestion, name: 'createQuotationRejectionQuestion'},

    {path: '/roles', component: ListRoles, name: 'listRoles'},
    {path: '/roles/create', component: CreateRoles, name: 'createRoles'},
    {path: '/roles/edit/:id', component: EditRoles, name: 'editRoles'},

    {path: '/activity_logs', component: ActivityLog, name: 'listActivityLog'},

    {path: '/requestForQuotation', component: RequestForQuotation, name: 'requestForQuotation'},
    {path: '/requestForQuotation/view/:id', component: ViewRequestForQuotation, name: 'viewRequestForQuotation'},
    {path: '/requestForQuotation/edit/:id', component: EditRequestForQuotation, name: 'editRequestForQuotation'},
    {path: '/pendingQuotations', component: PendingQuotation, name: 'pendingQuotations'},

    {path: '/priceComparison', component: PriceComparison, name: 'priceComparison'},
    {path: '/createPriceComparison/:id', component: CreatePriceComparison, name: 'createPriceComparison'},
    {path: '/editPriceComparison/:id', component: EditPriceComparison, name: 'editPriceComparison'},
    {path: '/viewPriceComparison/:id', component: ViewPriceComparison, name: 'viewPriceComparison'},

    {path: '/reviewQuotation/:id', component: ReviewQuotation, name: 'reviewQuotation'},

    {path: '/invoice/:id', component: Invoice, name: 'invoice'},
    {path: '/listInvoices', component: ListInvoices, name: 'listInvoices'},
    {path: '/followUpListing/:id', component: FollowUpListing, name: 'followUpListing'},
    {path: '/findQuotationById', component: FindQuotationById, name: 'findQuotationById'},

    {path: '/accounts', component: Accounts, name: 'accounts'},

    {path: '/leads', component: Leads, name: 'leads'},

    {path: '/expiredQuotations', component: ExpiredQuotations, name: 'expiredQuotations'},

    {path: '/pendingFollowups', component: PendingFollowups, name: 'pendingFollowups'},
]