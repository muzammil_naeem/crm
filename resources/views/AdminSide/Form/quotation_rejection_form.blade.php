@extends('Layout.guetslayout')

@section('content')
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">
                Quotation Rejection
            </h4>
            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
            <div class="heading-elements">
                <ul class="list-inline mb-0">
                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="card-content collapse show">
            <div class="card-body card-dashboard">
                <form action="{{route('postQuotaionRejectionForm',$data['quotation']['id'])}}" class="mt-1" method="post">
                    {{csrf_field()}}
                    <div class="form-body">
                        <h4 class="form-section"><i class="ft-user"></i>Quotation Rejection</h4>
                        @foreach($data['questions']->values()->chunk(2) as $chunks)
                            <div class="row">
                                @foreach($chunks as $question)
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>{{$question->question}}</label>
                                            <fieldset class="form-group position-relative has-icon-left">
                                                <textarea class="form-control" name="answers[{{$question->id}}]" placeholder="Write Here..."></textarea>
                                                <div class="form-control-position">
                                                    <i class="la la-user primary"></i>
                                                </div>
                                            </fieldset>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        @endforeach
                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn btn-success btn-glow float-right mb-2"><i class="la la-check-o"></i>Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
