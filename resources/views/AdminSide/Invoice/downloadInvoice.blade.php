<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="http://hitechhub.tech/crm/public/assets/AdminTheme/app-assets/css/vendors.css">
    <!-- END VENDOR CSS-->
    <!-- BEGIN MODERN CSS-->
    <link rel="stylesheet" type="text/css" href="http://hitechhub.tech/crm/public/assets/AdminTheme/app-assets/css/app.css">
    <!-- END Page Level CSS-->
</head>

<body style="background-color: white">
<div class="app-content content">
    <div class="content-wrapper">
        <!-- Invoice Company Details -->
        <div id="invoice-company-details" class="row">
            <div class="col-md-6">
                <div class="media">
                    <img src="http://hitechhub.tech/crm/public/assets/AdminTheme/app-assets/images/logo/logo.png" alt="company logo" class="">
                </div>
            </div>
            <div class="col-md-6 text-right">
                <h2 class="info"><strong>Quotation</strong></h2>
                <p class="pb-3"># {{$data['quotation']['quote_no']}}</p>
            </div>
        </div>
        <!--/ Invoice Company Details -->
        <!-- Invoice Customer Details -->
        <div id="invoice-customer-details" class="row pt-2">
            <div class="col-md-6 text-left">
                <p><strong class="text-muted info">{{ $data['customer']['first_name'].' '.$data['customer']['last_name']}}</strong></p>
                <p><strong class="text-muted info">{{ $data['customer']['customer_no'] }}</strong></p>
            </div>
            <div class="col-md-6 text-right">
                <p><span class="text-muted">Invoice Expiry:</span> {{$data['quotation']['quotation_expiry']}}</p>
            </div>
        </div>
        <!--/ Invoice Customer Details -->
        <!-- Invoice Items Details -->
        <div id="invoice-items-details" class="pt-2">
            <div class="row">
                <div class="table-responsive col-sm-12">
                    <table class="table">
                        <thead>
                        <tr class="bg-info text-white">
                            <th>Part Number</th>
                            <th>Part Name</th>
                            <th>Quantity</th>
                            <th>Condition</th>
                            <th>Price</th>
                            <th>Extended Price</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data['quotation']['parts'] as $key => $added_row)
                            <tr>
                                <td>{{ $added_row->part_no }}</td>
                                <td>{{ $added_row->name }}</td>
                                <td>{{ $added_row->quantity }}</td>
                                <td>{{ $added_row->condition }}</td>
                                <td>{{ $added_row->price }}</td>
                                <td>{{ $added_row->grand_extended_price }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-md-7 col-sm-12 text-center text-md-left">
                    <p class="lead"></p>
                    <div class="row">
                        <div class="col-md-8">
                            <table class="table table-borderless table-sm">
                                <tbody>
                                <tr>
                                    <td></td>
                                    <td class="text-right"></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td class="text-right"></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td class="text-right"></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td class="text-right"></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-md-5 col-sm-12">
                    <p class="lead ml-2 info"><strong>Total due</strong></p>
                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                            <tr>
                                <td>Sub Total</td>
                                <td class="text-right">{{ $data['quotation']['total'] }}</td>
                            </tr>
                            @if($data['quotation']['tax_percentage'] > 0)
                                <tr>
                                    <td>Tax Percentage</td>
                                    <td class="text-right"> {{ $data['quotation']['tax_percentage'] }}% </td>
                                </tr>
                            @endif
                            @if($data['quotation']['shipment_cost'] > 0)
                                <tr>
                                    <td>Shipment Charges</td>
                                    <td class="text-right"> {{ $data['quotation']['shipment_cost'] }} </td>
                                </tr>
                            @endif
                            @if($data['quotation']['credit_card_percentage'] > 0)
                                <tr>
                                    <td>Credit Card Percentage</td>
                                    <td class="text-right"> {{ $data['quotation']['credit_card_percentage'] }}% </td>
                                </tr>
                            @endif
                            <tr class="bg-info text-white">
                                <td class="text-bold-800"><strong>Grand Total</strong></td>
                                <td class="text-bold-800 text-right"><strong>{{  $data['quotation']['grand_total'] }}</strong> </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>



</html>
