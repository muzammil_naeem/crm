@extends('Layout.guetslayout')

@section('content')
    @if(Session::has('message') || $data['quotation']['accepted_by'] || in_array($data['quotation']['status'],['completed','rejected']))
        <div class="alert round bg-success alert-icon-left alert-arrow-left alert-dismissible mb-2"
             role="alert">
            <span class="alert-icon"><i class="la la-thumbs-o-up"></i></span>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong>{{ Session::get('message') ? Session::get('message') : 'Your Quotation Has Been '.ucwords($data['quotation']['status']).' !!!' }}</strong>
        </div>
    @elseif($data['quotation']['status'] == 'expired')
        <div class="alert round bg-warning alert-icon-left alert-arrow-left alert-dismissible mb-2"
             role="alert">
            <span class="alert-icon"><i class="la la-warning"></i></span>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong>Your Quotation Has Been Expired, You Can Re Request For Quotation By Clicking The Below Button !!!</strong>
        </div>
    @endif

    <section class="card">
        <div id="invoice-template" class="card-body">
            <!-- Invoice Company Details -->
            <div id="invoice-company-details" class="row">
                <div class="col-md-6 col-sm-12 text-center text-md-left">
                    <div class="media">
                        <img src="{{URL::asset("public/assets/AdminTheme/app-assets/images/logo/logo.png")}}" alt="company logo" class="">
                    </div>
                </div>
                <div class="col-md-6 col-sm-12 text-center text-md-right">
                    <h2>Quotation</h2>
                    <p class="pb-3"># {{$data['quotation']['quote_no']}}</p>
                </div>
            </div>
            <!--/ Invoice Company Details -->
            <!-- Invoice Customer Details -->
            <div id="invoice-customer-details" class="row pt-2">
                <div class="col-sm-12 text-center text-md-left">
                    <p class="text-muted">Bill To</p>
                </div>
                <div class="col-md-6 col-sm-12 text-center text-md-left">
                    <ul class="px-0 list-unstyled">
                        <li class="text-bold-800">{{ $data['customer']['first_name'],' '.$data['customer']['last_name']}}</li>
                        <li>{{ $data['customer']['customer_no'] }}</li>
                    </ul>
                </div>
                <div class="col-md-6 col-sm-12 text-center text-md-right mt-2">
                    <p><span class="text-muted">Invoice Expiry:</span> {{$data['quotation']['quotation_expiry']}}</p>
                </div>
            </div>
            <!--/ Invoice Customer Details -->
            <!-- Invoice Items Details -->
            <div id="invoice-items-details" class="pt-2">
                <div class="row">
                    <div class="table-responsive col-sm-12">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Part Number</th>
                                <th>Description</th>
                                <th>Quantity</th>
                                <th>Condition</th>
                                <th>Price</th>
                                <th>Extended Price</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($data['quotation']['parts'] as $key => $added_row)
                                <tr>
                                    <td>{{ $added_row->part_no }}</td>
                                    <td>{{ $added_row->name }}</td>
                                    <td>{{ $added_row->quantity }}</td>
                                    <td>{{ $added_row->condition }}</td>
                                    <td>{{ $added_row->price }}</td>
                                    <td>{{ $added_row->grand_extended_price }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-7 col-sm-12 text-center text-md-left">
                        <p class="lead"></p>
                        <div class="row">
                            <div class="col-md-8">
                                <table class="table table-borderless table-sm">
                                    <tbody>
                                    <tr>
                                        <td></td>
                                        <td class="text-right"></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td class="text-right"></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td class="text-right"></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td class="text-right"></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5 col-sm-12">
                        <p class="lead">Total due</p>
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                <tr>
                                    <td>Sub Total</td>
                                    <td class="text-right">{{ $data['quotation']['total'] }}</td>
                                </tr>
                                @if($data['quotation']['tax_percentage'] > 0)
                                    <tr>
                                        <td>Tax Percentage</td>
                                        <td class="text-right"> {{ $data['quotation']['tax_percentage'] }}% </td>
                                    </tr>
                                @endif
                                @if($data['quotation']['shipment_cost'] > 0)
                                    <tr>
                                        <td>Shipment Charges</td>
                                        <td class="text-right"> {{ $data['quotation']['shipment_cost'] }} </td>
                                    </tr>
                                @endif
                                @if($data['quotation']['credit_card_percentage'] > 0)
                                    <tr>
                                        <td>Credit Card Percentage</td>
                                        <td class="text-right"> {{ $data['quotation']['credit_card_percentage'] }}% </td>
                                    </tr>
                                @endif
                                <tr>
                                    <td class="text-bold-800">Grand Total</td>
                                    <td class="text-bold-800 text-right"> {{  $data['quotation']['grand_total'] }} </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Invoice Footer -->
            <div id="invoice-footer">
                <div class="row">
                    <div class="col-md-12 col-sm-12 text-center mt-3">
                        @if(empty($data['quotation']['accepted_by']) && !in_array($data['quotation']['status'],['completed','rejected','expired']))
                            <a class="btn btn-info acceptInvoiceModal btn-glow text-white float-right ml-1" ><i class="la la-check-o"></i>Accept</a>
                            <a href="{{route('getQuotaionRejectionForm',$data['quotation']['id'])}}" class="btn btn-info btn-glow float-right ml-1" ><i class="la la-check-o"></i>Reject</a>
                        @endif
                        @if($data['quotation']['status'] == 'expired')
                            <a href="{{route('reRequestForQuotation',$data['quotation']['id'])}}" class="btn btn-info btn-glow float-right ml-1" ><i class="la la-check-o"></i>Request For Quotation</a>
                        @else
                            <a href="{{route('downloadPdf',$data['quotation']['id'])}}" class="btn btn-info btn-glow float-right ml-1" ><i class="la la-check-o"></i>Download PDF</a>
                        @endif
                    </div>
                </div>
            </div>
            <!--/ Invoice Footer -->
        </div>
    </section>

    <div class="modal animated fadeIn text-left MyModal" id="acceptInvoiceModal">
        <form action="{{route('acceptInvoiceCustomer',$data['quotation']['id'])}}" method="post">
            {{csrf_field()}}
            <div class="modal-dialog modal-lg"> <!-- modal size class needs to be added in this -->
                <div class="modal-content">
                    <div class="modal-header bg-info"> <!-- background class needs to be added in this -->
                        <h4 class="modal-title text-white">
                            Accept Invoice
                        </h4>
                    </div>

                    <div class="modal-body">
                        <div class="form-group">
                            <label>Comment</label>
                            <fieldset class="form-group position-relative has-icon-left">
                                <textarea class="form-control" name="comments" placeholder="Enter Comments Here..."></textarea>
                                <div class="form-control-position">
                                    <i class="la la-info-circle info"></i>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-info pull-right" data-dismiss="modal" aria-label="Close">Cancel</button>
                        <button type="submit" class="btn btn-info pull-right">Accept</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <script>
        $(document).ready(function(){
            $('.acceptInvoiceModal').on('click',function () {
                $('#acceptInvoiceModal').modal('show');
            });
        });
    </script>

@endsection
