<div class="alert round bg-cyan alert-icon-left alert-arrow-left alert-dismissible mb-2" role="alert">
    <span class="alert-icon"><i class="la la-photo"></i></span>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    <strong>{{ $text }}</strong>
</div>