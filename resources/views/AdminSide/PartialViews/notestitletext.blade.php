<div class="media-list media-bordered" v-for="(note, index) in notes">
    <div class="media border-left-red border-left-2 mt-1 mb-1">
        <span class="media-left">
          <img class="media-object rounded-circle" src="{{URL::asset("assets/AdminTheme/app-assets/images/portrait/small/avatar-s-4.png")}}" alt="Generic placeholder image" style="width: 64px;height: 64px;"/>
        </span>
        <div class="media-body">
            <h5 class="media-heading">@{{ note.title }}</h5>
            @{{ note.notestext }}
            <div class="media-notation mt-1">
                <a class="tooltip-primary" title="Edit Notes"><i class="la la-pencil mr-0 text-primary"></i></a>
                <a @click="delete_notesRow(index)" class="tooltip-danger" title="Delete Notes"><i class="ft-trash-2 mr-0 text-danger"></i></a>
                <label class="pull-right text-info"><i class="la la-clock-o"></i> 14 hrs ago by "Owner Name"</label>
            </div>
        </div>
    </div>
</div>