
<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="CRM">
    <meta name="keywords" content="admin template, modern admin template, dashboard template, flat admin template, responsive admin template, web app, crypto dashboard, bitcoin dashboard">
    <meta name="author" content="CRM">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name','CRM') }}</title>
    <link rel="apple-touch-icon" href="{{--{{URL::asset("assets/AdminTheme/app-assets/images/ico/apple-icon-120.png")}}--}}">
    <link rel="shortcut icon" type="image/x-icon" href="{{URL::asset("public/assets/AdminTheme/app-assets/images/ico/favicon.ico")}}"> <!-- change logo here -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Quicksand:300,400,500,700" rel="stylesheet">
    <link href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome.min.css" rel="stylesheet">
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="{{URL::asset("public/assets/AdminTheme/app-assets/css/vendors.css")}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset("public/assets/AdminTheme/app-assets/vendors/css/extensions/sweetalert.css")}}">
    <!-- END VENDOR CSS-->
    <!-- BEGIN MODERN CSS-->
    <link rel="stylesheet" type="text/css" href="{{URL::asset("public/assets/AdminTheme/app-assets/css/app.css")}}">

    <!-- END MODERN CSS-->
    <!-- BEGIN Page Level CSS-->
{{--    <link rel="stylesheet" type="text/css" href="{{URL::asset("public/assets/AdminTheme/app-assets/css/core/menu/menu-types/vertical-menu.css")}}">--}}
    <!-- END Page Level CSS-->

    <!-- CSS ToolTip Start-->
    <link rel="stylesheet" type="text/css" href="{{URL::asset("public/assets/AdminTheme/app-assets/vendors/css/ui/jquery-ui.min.css")}}">
    <!-- CSS ToolTip End-->

    <!-- Datatable Start-->
    <link rel="stylesheet" type="text/css" href="{{URL::asset("public/assets/AdminTheme/app-assets/vendors/css/tables/datatable/datatables.min.css")}}">
    <!-- Datatable End-->

    <!-- DatePicker Start-->
{{--    <link rel="stylesheet" type="text/css" href="{{URL::asset("public/assets/AdminTheme/app-assets/vendors/css/pickers/pickadate/pickadate.css")}}">--}}
{{--    <link rel="stylesheet" type="text/css" href="{{URL::asset("public/assets/AdminTheme/app-assets/vendors/css/pickers/daterange/daterangepicker.css")}}">--}}
    <!-- DatePicker End-->

    <!-- International Tel Input Start-->
{{--    <link rel="stylesheet" type="text/css" href="{{URL::asset("public/assets/IntTelInput/css/intlTelInput.css")}}">--}}
    <!-- International Tel Input End-->

    <!-- Gloabl Style Start-->
{{--    <link rel="stylesheet" type="text/css" href="{{URL::asset("public/assets/Style/globalstyle.css")}}">--}}
    <!-- Gloabl Style End-->

    <!-- Select2 Start-->
    <link rel="stylesheet" type="text/css" href="{{URL::asset("public/assets/AdminTheme/app-assets/vendors/css/forms/selects/select2.min.css")}}">
    <!-- Select2 End-->

    <!-- Line Checkbox Start-->
{{--    <link rel="stylesheet" type="text/css" href="{{URL::asset("public/assets/AdminTheme/app-assets/vendors/css/forms/icheck/icheck.css")}}">--}}
    <!-- Line Checkbox End-->

    <!-- Toastr Start-->
{{--    <link rel="stylesheet" type="text/css" href="{{URL::asset("public/assets/AdminTheme/app-assets/vendors/css/extensions/toastr.css")}}">--}}
{{--    <link rel="stylesheet" type="text/css" href="{{URL::asset("public/assets/AdminTheme/app-assets/css/plugins/extensions/toastr.css")}}">--}}
    <!-- Toastr End-->

    <!-- Dropzone Start-->
{{--    <link rel="stylesheet" type="text/css" href="{{URL::asset("public/assets/AdminTheme/app-assets/vendors/css/file-uploaders/dropzone.min.css")}}">--}}
{{--    <link rel="stylesheet" type="text/css" href="{{URL::asset("public/assets/AdminTheme/app-assets/css/plugins/file-uploaders/dropzone.css")}}">--}}
    <!-- Dropzone End-->

    <!-- Animate Start-->
    <link rel="stylesheet" type="text/css" href="{{URL::asset("public/assets/AdminTheme/app-assets/css/plugins/animate/animate.css")}}">
    <!-- Animate End-->

    <!-- Spinner Start-->
{{--    <link rel="stylesheet" type="text/css" href="{{URL::asset("public/assets/AdminTheme/app-assets/css/plugins/loaders/loaders.min.css")}}">--}}
    <link rel="stylesheet" type="text/css" href="{{URL::asset("public/assets/AdminTheme/app-assets/css/core/colors/palette-loader.css")}}">
    <!-- Spinner End-->

    <link rel="stylesheet" type="text/css" href="{{URL::asset("public/assets/AdminTheme/app-assets/css/core/colors/palette-callout.css")}}">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
</head>

<body class="vertical-layout fixed-navbar">

<!-- fixed-top-->
<nav class="header-navbar navbar-expand-md navbar navbar-with-menu navbar-without-dd-arrow fixed-top navbar-semi-light bg-info navbar-shadow">
    <div class="navbar-wrapper">
    </div>
</nav>

<!-- ////////////////////////////////////////////////////////////////////////////-->

<div class="app-content content">
    <div class="content-wrapper">
        @yield('content')
    </div>
</div>

<!-- ////////////////////////////////////////////////////////////////////////////-->


<footer class="footer footer-static footer-light navbar-border navbar-shadow">
    <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2 float-md-right">
        <span class="float-md-left d-block d-md-inline-block">Copyright  &copy; 2018
            <a class="text-bold-800 grey darken-2">Company Name</a>
            , All rights reserved.
        </span>
    </p>
</footer>

<!-- BEGIN VUE JS INTIALIZE-->
<!-- END VUE JS INTIALIZE-->

<!-- BEGIN VENDOR JS-->
<script src="{{URL::asset("public/assets/AdminTheme/app-assets/js/core/libraries/jquery.min.js")}}" type="text/javascript"></script>
{{--<script src="{{URL::asset("public/assets/AdminTheme/app-assets/vendors/js/ui/popper.min.js")}}" type="text/javascript"></script>--}}
<script src="{{URL::asset("public/assets/AdminTheme/app-assets/js/core/libraries/bootstrap.min.js")}}" type="text/javascript"></script>
{{--<script src="{{URL::asset("public/assets/AdminTheme/app-assets/vendors/js/ui/perfect-scrollbar.jquery.min.js")}}" type="text/javascript"></script>--}}
{{--<script src="{{URL::asset("public/assets/AdminTheme/app-assets/vendors/js/ui/unison.min.js")}}" type="text/javascript"></script>--}}
{{--<script src="{{URL::asset("public/assets/AdminTheme/app-assets/vendors/js/ui/blockUI.min.js")}}" type="text/javascript"></script>--}}
{{--<script src="{{URL::asset("public/assets/AdminTheme/app-assets/vendors/js/ui/jquery-sliding-menu.js")}}" type="text/javascript"></script>--}}
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<script src="{{URL::asset('public/assets/AdminTheme/app-assets/vendors/js/extensions/sweetalert.min.js')}}" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN MODERN JS-->
<!--<script src="{{URL::asset("public/assets/AdminTheme/app-assets/js/core/app-menu.js")}}" type="text/javascript"></script>-->
<script src="{{URL::asset("public/assets/AdminTheme/app-assets/js/core/app.js")}}" type="text/javascript"></script>
<!-- END MODERN JS-->
<!-- BEGIN PAGE LEVEL JS-->
<script src="{{URL::asset('public/assets/AdminTheme/app-assets/js/scripts/extensions/sweet-alerts.js')}}" type="text/javascript"></script>
<!-- END PAGE LEVEL JS-->

<!-- Tooltip JS Start-->
{{--<script src="{{URL::asset("public/assets/AdminTheme/app-assets/js/scripts/ui/jquery-ui/dialog-tooltip.js")}}" type="text/javascript"></script>--}}
<script src="{{URL::asset("public/assets/AdminTheme/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js")}}" type="text/javascript"></script>
<!-- Tooltip JS End-->

<!-- Datatable JS Start-->
{{--<script src="{{URL::asset("public/assets/AdminTheme/app-assets/vendors/js/tables/datatable/datatables.min.js")}}" type="text/javascript"></script>--}}
{{--<script src="{{URL::asset("public/assets/AdminTheme/app-assets/js/scripts/tables/datatables/datatable-basic.js")}}" type="text/javascript"></script>--}}
<!-- Datatable JS End-->

<!-- DatePicker JS Start-->
{{--<script src="{{URL::asset("public/assets/AdminTheme/app-assets/vendors/js/pickers/pickadate/picker.js")}}" type="text/javascript"></script>--}}
{{--<script src="{{URL::asset("public/assets/AdminTheme/app-assets/vendors/js/pickers/pickadate/picker.date.js")}}" type="text/javascript"></script>--}}
{{--<script src="{{URL::asset("public/assets/AdminTheme/app-assets/vendors/js/pickers/pickadate/picker.time.js")}}" type="text/javascript"></script>--}}
{{--<script src="{{URL::asset("public/assets/AdminTheme/app-assets/vendors/js/pickers/pickadate/legacy.js")}}" type="text/javascript"></script>--}}
{{--<script src="{{URL::asset("public/assets/AdminTheme/app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js")}}" type="text/javascript"></script>--}}
{{--<script src="{{URL::asset("public/assets/AdminTheme/app-assets/vendors/js/pickers/daterange/daterangepicker.js")}}" type="text/javascript"></script>--}}
{{--<script src="{{URL::asset("public/assets/AdminTheme/app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js")}}" type="text/javascript"></script>--}}
<!-- DatePicker JS End-->

<!-- International Tel Input Start-->
{{--<script src="{{URL::asset("public/assets/IntTelInput/js/intlTelInput.js")}}" type="text/javascript"></script>--}}
<!-- International Tel Input End-->

<!-- Gloabl Sript Start-->
{{--<script src="{{URL::asset("public/assets/Script/globalscript.js")}}" type="text/javascript"></script>--}}
<!-- Gloabl Sript End-->

<!-- Select2 Start-->
{{--<script src="{{URL::asset("public/assets/AdminTheme/app-assets/vendors/js/forms/select/select2.full.min.js")}}" type="text/javascript"></script>--}}
{{--<script src="{{URL::asset("public/assets/AdminTheme/app-assets/js/scripts/forms/select/form-select2.js")}}" type="text/javascript"></script>--}}
<!-- Select2 End-->

<!-- Line Checkbox Start-->
{{--<script src="{{URL::asset("public/assets/AdminTheme/app-assets/vendors/js/forms/icheck/icheck.min.js")}}" type="text/javascript"></script>--}}
<!-- Line Checkbox End-->

<!-- Toastr Start-->
{{--<script src="{{URL::asset("public/assets/AdminTheme/app-assets/vendors/js/extensions/toastr.min.js")}}" type="text/javascript"></script>--}}
<!-- Toastr End-->

<!-- Dropzone Start-->
{{--<script src="{{URL::asset("public/assets/AdminTheme/app-assets/vendors/js/extensions/dropzone.min.js")}}" type="text/javascript"></script>
<script src="{{URL::asset("public/assets/AdminTheme/app-assets/js/scripts/extensions/dropzone.js")}}" type="text/javascript"></script>--}}
{{--<script type="text/javascript">Dropzone.autoDiscover = false;</script>--}}
<!-- Dropzone End-->

<!-- Modal Start-->
<script src="{{URL::asset("public/assets/AdminTheme/app-assets/js/scripts/modal/components-modal.js")}}" type="text/javascript"></script>
<!-- Modal End-->

</body>



</html>
