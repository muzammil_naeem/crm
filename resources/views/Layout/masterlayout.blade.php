
<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="CRM">
    <meta name="keywords" content="admin template, modern admin template, dashboard template, flat admin template, responsive admin template, web app, crypto dashboard, bitcoin dashboard">
    <meta name="author" content="CRM">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name','CRM') }}</title>
    <link rel="apple-touch-icon" href="{{--{{URL::asset("assets/AdminTheme/app-assets/images/ico/apple-icon-120.png")}}--}}">
    <link rel="shortcut icon" type="image/x-icon" href="{{URL::asset("public/assets/AdminTheme/app-assets/images/logo/logo.png")}}"> <!-- change logo here -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Quicksand:300,400,500,700" rel="stylesheet">
    <link href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome.min.css" rel="stylesheet">
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="{{URL::asset("public/assets/AdminTheme/app-assets/css/vendors.css")}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset("public/assets/AdminTheme/app-assets/vendors/css/extensions/sweetalert.css")}}">
    <!-- END VENDOR CSS-->
    <!-- BEGIN MODERN CSS-->
    <link rel="stylesheet" type="text/css" href="{{URL::asset("public/assets/AdminTheme/app-assets/css/app.css")}}">

    <!-- END MODERN CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="{{URL::asset("public/assets/AdminTheme/app-assets/css/core/menu/menu-types/vertical-menu.css")}}">
    <!-- END Page Level CSS-->

    <!-- CSS ToolTip Start-->
    <link rel="stylesheet" type="text/css" href="{{URL::asset("public/assets/AdminTheme/app-assets/vendors/css/ui/jquery-ui.min.css")}}">
    <!-- CSS ToolTip End-->

    <!-- Datatable Start-->
    <link rel="stylesheet" type="text/css" href="{{URL::asset("public/assets/AdminTheme/app-assets/vendors/css/tables/datatable/datatables.min.css")}}">
    <!-- Datatable End-->

    <!-- DatePicker Start-->
    <link rel="stylesheet" type="text/css" href="{{URL::asset("public/assets/AdminTheme/app-assets/vendors/css/pickers/pickadate/pickadate.css")}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset("public/assets/AdminTheme/app-assets/vendors/css/pickers/daterange/daterangepicker.css")}}">
    <!-- DatePicker End-->

    <!-- International Tel Input Start-->
    <link rel="stylesheet" type="text/css" href="{{URL::asset("public/assets/IntTelInput/css/intlTelInput.css")}}">
    <!-- International Tel Input End-->

    <!-- Gloabl Style Start-->
    <link rel="stylesheet" type="text/css" href="{{URL::asset("public/assets/Style/globalstyle.css")}}">
    <!-- Gloabl Style End-->

    <!-- Select2 Start-->
    <link rel="stylesheet" type="text/css" href="{{URL::asset("public/assets/AdminTheme/app-assets/vendors/css/forms/selects/select2.min.css")}}">
    <!-- Select2 End-->

    <!-- Line Checkbox Start-->
    <link rel="stylesheet" type="text/css" href="{{URL::asset("public/assets/AdminTheme/app-assets/vendors/css/forms/icheck/icheck.css")}}">
    <!-- Line Checkbox End-->

    <!-- Toastr Start-->
        <link rel="stylesheet" type="text/css" href="{{URL::asset("public/assets/AdminTheme/app-assets/vendors/css/extensions/toastr.css")}}">
        <link rel="stylesheet" type="text/css" href="{{URL::asset("public/assets/AdminTheme/app-assets/css/plugins/extensions/toastr.css")}}">
    <!-- Toastr End-->

    <!-- Dropzone Start-->
        <link rel="stylesheet" type="text/css" href="{{URL::asset("public/assets/AdminTheme/app-assets/vendors/css/file-uploaders/dropzone.min.css")}}">
        <link rel="stylesheet" type="text/css" href="{{URL::asset("public/assets/AdminTheme/app-assets/css/plugins/file-uploaders/dropzone.css")}}">
    <!-- Dropzone End-->

    <!-- Animate Start-->
        <link rel="stylesheet" type="text/css" href="{{URL::asset("public/assets/AdminTheme/app-assets/css/plugins/animate/animate.css")}}">
    <!-- Animate End-->

    <!-- Spinner Start-->
    <link rel="stylesheet" type="text/css" href="{{URL::asset("public/assets/AdminTheme/app-assets/css/plugins/loaders/loaders.min.css")}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset("public/assets/AdminTheme/app-assets/css/core/colors/palette-loader.css")}}">
    <!-- Spinner End-->

    <link rel="stylesheet" type="text/css" href="{{URL::asset("public/assets/AdminTheme/app-assets/css/core/colors/palette-callout.css")}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset("public/assets/AdminTheme/app-assets/css/core/colors/palette-gradient.css")}}">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

    <script type="text/javascript">
        @auth
            window.Permissions = {!! json_encode(Illuminate\Support\Facades\Auth::user()->allPermissions, true) !!};
        @else
            window.Permissions = [];
        @endauth
    </script>
</head>

<body class="vertical-layout vertical-menu 2-columns menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu" data-col="2-columns">

<!-- fixed-top-->
<span id="app-vue">
    <nav class="header-navbar navbar-expand-md navbar navbar-with-menu navbar-without-dd-arrow fixed-top navbar-semi-light bg-info navbar-shadow">
        <div class="navbar-wrapper">
            <div class="navbar-header">
                <ul class="nav navbar-nav flex-row">
                    <li class="nav-item mobile-menu d-md-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu font-large-1"></i></a></li>
                    <li class="nav-item"><a class="navbar-brand" href="{{route('dashboard')}}"><img class="brand-logo" alt="modern admin logo" src="{{URL::asset("public/assets/AdminTheme/app-assets/images/logo/logo.png")}}"><!-- change logo here -->
                            <h3 class="brand-text">CRM</h3></a></li>
                    <li class="nav-item d-md-none"><a class="nav-link open-navbar-container" data-toggle="collapse" data-target="#navbar-mobile"><i class="la la-ellipsis-v"></i></a></li>
                </ul>
            </div>
            <div class="navbar-container content">
                <div class="collapse navbar-collapse" id="navbar-mobile">
                    <ul class="nav navbar-nav mr-auto float-left">
                        <li class="nav-item d-none d-md-block"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu"></i></a></li>
                    </ul>
                    <ul class="nav navbar-nav float-right">
                        <li class="dropdown dropdown-user nav-item custom-nav-dropdown">
                            <a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown"><span class="avatar avatar-online"><img src="{{URL::asset("public/assets/AdminTheme/app-assets/images/portrait/small/avatar-s-19.png")}}" alt="avatar"><i></i></span><span class="user-name">{{auth()->user()->first_name.' '.auth()->user()->last_name}}</span></a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="#/sales_person/edit/{{auth()->user()->id}}"><i class="ft-user"></i> Edit Profile</a>
                                <div class="dropdown-divider"></div><a class="dropdown-item" href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="ft-power c-red text-danger"></i> Logout</a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>

    <!-- ////////////////////////////////////////////////////////////////////////////-->


    <div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
        <div class="main-menu-content">
            <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
                <li class=" nav-item">
                    <router-link class="menu-item tooltip-info" :to="{name:'dashboard'}"><i class="la la-inbox"></i>Dashboard</router-link>
                </li>
                @if(auth()->user()->can('customer-create') || auth()->user()->can('customer-list'))
                    <li class=" nav-item">
                        <a href="#"><i class="la la-user"></i><span class="menu-title" data-i18n="">Customer</span></a>

                        <ul class="menu-content">
                            @if(auth()->user()->can('customer-create'))
                                <li>
                                    <router-link class="menu-item tooltip-info" :to="{name:'createCustomer'}"><i class="la la-user-plus"></i>Create Customer</router-link>
                                </li>
                            @endif
                            @if(auth()->user()->can('customer-list'))
                                <li>
                                    <router-link class="menu-item tooltip-info" :to="{name:'listCustomers'}"><i class="la la-th-list"></i>List Customer</router-link>
                                </li>
                            @endif
                        </ul>
                    </li>
                @endif

                @if(auth()->user()->can('request_for_quotation-list') || auth()->user()->can('price_comparison-list') || auth()->user()->can('invoice-list')
                    || auth()->user()->can('invoice-list') || auth()->user()->can('follow_up-pending-follow-ups') || auth()->user()->can('expired_quotations-list')
                    || auth()->user()->can('find_quotation_by_id-view'))
                    <li class=" nav-item">
                        <a href="#"><i class="la la-object-ungroup"></i><span class="menu-title" data-i18n="">Quotation</span></a>
                        <ul class="menu-content">
                            @if(auth()->user()->can('request_for_quotation-list'))
                                <li>
                                    <router-link class="menu-item tooltip-info" :to="{name:'pendingQuotations'}"><i class="la la-plus-square"></i>Request For Quotation</router-link>
                                </li>
                            @endif
                            @if(auth()->user()->can('price_comparison-list'))
                                <li>
                                    <router-link class="menu-item tooltip-info" :to="{name:'priceComparison'}"><i class="la la-money"></i>Price Comparison</router-link>
                                </li>
                            @endif
                            @if(auth()->user()->can('invoice-list'))
                                <li>
                                    <router-link class="menu-item tooltip-info" :to="{name:'listInvoices'}"><i class="la la-th-list"></i>Invoices</router-link>
                                </li>
                            @endif
                            @if(auth()->user()->can('follow_up-pending-follow-ups'))
                                <li>
                                    <router-link class="menu-item tooltip-info" :to="{name:'pendingFollowups'}"><i class="ft-clock"></i>Pending Follow Ups</router-link>
                                </li>
                            @endif
                            @if(auth()->user()->can('expired_quotations-list'))
                                <li>
                                    <router-link class="menu-item tooltip-info" :to="{name:'expiredQuotations'}"><i class="la la-flag-o"></i>Expired Quotations</router-link>
                                </li>
                            @endif
                            @if(auth()->user()->can('find_quotation_by_id-view'))
                                <li>
                                    <router-link class="menu-item tooltip-info" :to="{name:'findQuotationById'}"><i class="la la-search"></i>Find Quotation</router-link>
                                </li>
                            @endif
                        </ul>
                    </li>
                @endif
                @if(auth()->user()->can('accounts-list'))
                    <li class="nav-item">
                        <router-link class="tooltip-info" :to="{name:'accounts'}"><i class="la la-money"></i>
                            Accounts
                        </router-link>
                    </li>
                @endif
                @if(auth()->user()->can('leads-list'))
                    <li class="nav-item">
                        <router-link class="tooltip-info" :to="{name:'leads'}"><i class="la la-tags"></i>
                            Leads
                        </router-link>
                    </li>
                @endif

                @if(auth()->user()->can('sales_person-create') || auth()->user()->can('sales_person-list')
                    || auth()->user()->can('roles-create') || auth()->user()->can('roles-list')
                    || auth()->user()->can('parts-create') || auth()->user()->can('parts-list')
                    || auth()->user()->can('rejection_questions-list') || auth()->user()->can('rejection_questions-create')
                    || auth()->user()->can('activity_log-list'))

                    <li class="navigation-header"><span>Configuration</span><i class="la la-ellipsis-h ft-minus tooltip-info"></i>

                    @if(auth()->user()->can('roles-create') || auth()->user()->can('roles-list'))
                        <li class=" nav-item">
                            <a href="#"><i class="la la-hand-stop-o"></i><span class="menu-title" data-i18n="">Role</span></a>
                            <ul class="menu-content">
                                @if(auth()->user()->can('roles-create'))
                                    <li>
                                        <router-link class="menu-item tooltip-info" :to="{name:'createRoles'}"><i class="la la-plus-square"></i>Create Role</router-link>
                                    </li>
                                @endif
                                @if(auth()->user()->can('roles-list'))
                                    <li>
                                        <router-link class="menu-item tooltip-info" :to="{name:'listRoles'}"><i class="la la-user-secret"></i>Manage Role Permission</router-link>
                                    </li>
                                @endif
                            </ul>
                        </li>
                    @endif

                    @if(auth()->user()->can('sales_person-create') || auth()->user()->can('sales_person-list'))
                        <li class=" nav-item">
                            <a href="#"><i class="la la-user"></i><span class="menu-title" data-i18n="">Sales Person</span></a>

                            <ul class="menu-content">
                                @if(auth()->user()->can('sales_person-create'))
                                    <li>
                                        <router-link class="menu-item tooltip-info" :to="{name:'createSalesPerson'}"><i class="la la-user-plus"></i>Create Sales Person</router-link>
                                    </li>
                                @endif
                                @if(auth()->user()->can('sales_person-list'))
                                    <li>
                                        <router-link class="menu-item tooltip-info" :to="{name:'listSalesPerson'}"><i class="la la-users"></i>Manage Sales Person</router-link>
                                    </li>
                                @endif
                            </ul>
                        </li>
                    @endif

                    @if(auth()->user()->can('parts-create') || auth()->user()->can('parts-list'))
                        <li class=" nav-item">
                            <a href="#"><i class="la la-gear"></i><span class="menu-title" data-i18n="">Parts</span></a>
                            <ul class="menu-content">
                                @if(auth()->user()->can('parts-create'))
                                    <li>
                                        <router-link :to="{name:'createPart'}" class="menu-item tooltip-info"><i class="la la-plus-square"></i>Create Parts</router-link>
                                    </li>
                                @endif
                                @if(auth()->user()->can('parts-list'))
                                    <li>
                                        <router-link :to="{name:'listParts'}" class="menu-item tooltip-info"><i class="la la-th-list"></i>Manage Parts</router-link>
                                    </li>
                                @endif
                            </ul>
                        </li>
                    @endif
                    @if(auth()->user()->can('rejection_questions-list') || auth()->user()->can('rejection_questions-create'))
                        <li class=" nav-item">
                            <a href="#"><i class="la la-question-circle"></i><span class="menu-title" data-i18n="">Rejection Questions</span></a>
                            <ul class="menu-content">
                                @if(auth()->user()->can('rejection_questions-create'))
                                    <li>
                                        <router-link :to="{name:'createQuotationRejectionQuestion'}" class="menu-item tooltip-info"><i class="la la-plus-square"></i>Create Questions</router-link>
                                    </li>
                                @endif
                                @if(auth()->user()->can('rejection_questions-list'))
                                    <li>
                                        <router-link :to="{name:'listQuotationRejectionQuestion'}" class="menu-item tooltip-info"><i class="la la-th-list"></i>Manage Questions</router-link>
                                    </li>
                                @endif
                            </ul>
                        </li>
                    @endif
                    @if(auth()->user()->can('activity_log-list'))
                        <li class="nav-item">
                            <router-link class="tooltip-info" :to="{name:'listActivityLog'}"><i class="ft-activity"></i>
                                Activity Log
                            </router-link>
                        </li>
                    @endif
                @endif
            </ul>
        </div>
    </div>

    <div class="app-content content">
        <div class="content-wrapper">
            @yield('content')
            <router-view></router-view>
        </div>
    </div>

    <!-- ////////////////////////////////////////////////////////////////////////////-->


    <footer class="footer footer-static footer-light navbar-border navbar-shadow">
        <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2 float-md-right">
        <span class="float-md-left d-block d-md-inline-block">Copyright  &copy; 2018
            <a class="text-bold-800 grey darken-2">Company Name</a>
            , All rights reserved.
        </span>
        </p>
    </footer>

</span>

<!-- BEGIN VUE JS INTIALIZE-->
<!-- END VUE JS INTIALIZE-->

<!-- BEGIN VENDOR JS-->
<script src="{{URL::asset("public/assets/AdminTheme/app-assets/js/core/libraries/jquery.min.js")}}" type="text/javascript"></script>
<script src="{{URL::asset("public/js/app.js")}}"></script>
<script src="{{URL::asset("public/assets/AdminTheme/app-assets/vendors/js/ui/popper.min.js")}}" type="text/javascript"></script>
<script src="{{URL::asset("public/assets/AdminTheme/app-assets/js/core/libraries/bootstrap.min.js")}}" type="text/javascript"></script>
<script src="{{URL::asset("public/assets/AdminTheme/app-assets/vendors/js/ui/perfect-scrollbar.jquery.min.js")}}" type="text/javascript"></script>
<script src="{{URL::asset("public/assets/AdminTheme/app-assets/vendors/js/ui/unison.min.js")}}" type="text/javascript"></script>
{{--<script src="{{URL::asset("public/assets/AdminTheme/app-assets/vendors/js/ui/blockUI.min.js")}}" type="text/javascript"></script>--}}
{{--<script src="{{URL::asset("public/assets/AdminTheme/app-assets/vendors/js/ui/jquery-sliding-menu.js")}}" type="text/javascript"></script>--}}
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<script src="{{URL::asset('public/assets/AdminTheme/app-assets/vendors/js/extensions/sweetalert.min.js')}}" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN MODERN JS-->
<script src="{{URL::asset("public/assets/AdminTheme/app-assets/js/core/app-menu.js")}}" type="text/javascript"></script>
<script src="{{URL::asset("public/assets/AdminTheme/app-assets/js/core/app.js")}}" type="text/javascript"></script>
<!-- END MODERN JS-->
<!-- BEGIN PAGE LEVEL JS-->
<script src="{{URL::asset('public/assets/AdminTheme/app-assets/js/scripts/extensions/sweet-alerts.js')}}" type="text/javascript"></script>
<!-- END PAGE LEVEL JS-->

<!-- Tooltip JS Start-->
{{--<script src="{{URL::asset("public/assets/AdminTheme/app-assets/js/scripts/ui/jquery-ui/dialog-tooltip.js")}}" type="text/javascript"></script>--}}
<script src="{{URL::asset("public/assets/AdminTheme/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js")}}" type="text/javascript"></script>
<!-- Tooltip JS End-->

<!-- Datatable JS Start-->
{{--<script src="{{URL::asset("public/assets/AdminTheme/app-assets/vendors/js/tables/datatable/datatables.min.js")}}" type="text/javascript"></script>--}}
{{--<script src="{{URL::asset("public/assets/AdminTheme/app-assets/js/scripts/tables/datatables/datatable-basic.js")}}" type="text/javascript"></script>--}}
<!-- Datatable JS End-->

<!-- DatePicker JS Start-->
{{--<script src="{{URL::asset("public/assets/AdminTheme/app-assets/vendors/js/pickers/pickadate/picker.js")}}" type="text/javascript"></script>--}}
{{--<script src="{{URL::asset("public/assets/AdminTheme/app-assets/vendors/js/pickers/pickadate/picker.date.js")}}" type="text/javascript"></script>--}}
{{--<script src="{{URL::asset("public/assets/AdminTheme/app-assets/vendors/js/pickers/pickadate/picker.time.js")}}" type="text/javascript"></script>--}}
{{--<script src="{{URL::asset("public/assets/AdminTheme/app-assets/vendors/js/pickers/pickadate/legacy.js")}}" type="text/javascript"></script>--}}
{{--<script src="{{URL::asset("public/assets/AdminTheme/app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js")}}" type="text/javascript"></script>--}}
{{--<script src="{{URL::asset("public/assets/AdminTheme/app-assets/vendors/js/pickers/daterange/daterangepicker.js")}}" type="text/javascript"></script>--}}
{{--<script src="{{URL::asset("public/assets/AdminTheme/app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js")}}" type="text/javascript"></script>--}}
<!-- DatePicker JS End-->

<!-- International Tel Input Start-->
{{--<script src="{{URL::asset("public/assets/IntTelInput/js/intlTelInput.js")}}" type="text/javascript"></script>--}}
<!-- International Tel Input End-->

<!-- Gloabl Sript Start-->
{{--<script src="{{URL::asset("public/assets/Script/globalscript.js")}}" type="text/javascript"></script>--}}
<!-- Gloabl Sript End-->

<!-- Select2 Start-->
<script src="{{URL::asset("public/assets/AdminTheme/app-assets/vendors/js/forms/select/select2.full.min.js")}}" type="text/javascript"></script>
<script src="{{URL::asset("public/assets/AdminTheme/app-assets/js/scripts/forms/select/form-select2.js")}}" type="text/javascript"></script>
<!-- Select2 End-->

<!-- Line Checkbox Start-->
{{--<script src="{{URL::asset("public/assets/AdminTheme/app-assets/vendors/js/forms/icheck/icheck.min.js")}}" type="text/javascript"></script>--}}
<!-- Line Checkbox End-->

<!-- Toastr Start-->
<script src="{{URL::asset("public/assets/AdminTheme/app-assets/vendors/js/extensions/toastr.min.js")}}" type="text/javascript"></script>
<!-- Toastr End-->

<!-- Dropzone Start-->
{{--<script src="{{URL::asset("public/assets/AdminTheme/app-assets/vendors/js/extensions/dropzone.min.js")}}" type="text/javascript"></script>
<script src="{{URL::asset("public/assets/AdminTheme/app-assets/js/scripts/extensions/dropzone.js")}}" type="text/javascript"></script>--}}
{{--<script type="text/javascript">Dropzone.autoDiscover = false;</script>--}}
<!-- Dropzone End-->

<!-- Modal Start-->
<script src="{{URL::asset("public/assets/AdminTheme/app-assets/js/scripts/modal/components-modal.js")}}" type="text/javascript"></script>
<!-- Modal End-->

</body>



</html>
