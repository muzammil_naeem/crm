<html class="loaded" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name=viewport content='width=700'>

    <meta name="description" content="Modern admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities with bitcoin dashboard.">
    <meta name="keywords" content="admin template, modern admin template, dashboard template, flat admin template, responsive admin template, web app, crypto dashboard, bitcoin dashboard">
    <meta name="author" content="PIXINVENT">

    <title>{{ config('app.name') }}</title>

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="apple-touch-icon" href="{{URL::asset('public/assets/AdminTheme/app-assets/images/ico/apple-icon-120.png')}}">
    <link rel="shortcut icon" type="image/x-icon" href="{{URL::asset("public/assets/AdminTheme/app-assets/images/logo/logo.png")}}">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Quicksand:300,400,500,700" rel="stylesheet">
    <link href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome.min.css" rel="stylesheet">

    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="{{URL::asset('public/assets/AdminTheme/app-assets/css/vendors.css')}}">
    <!-- END VENDOR CSS-->

    <!-- BEGIN MODERN CSS-->
    <link rel="stylesheet" type="text/css" href="{{URL::asset('public/assets/AdminTheme/app-assets/css/app.css')}}">
    <!-- END MODERN CSS-->

    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="{{URL::asset('public/assets/AdminTheme/app-assets/css/pages/login-register.css')}}">
    <!-- END Page Level CSS-->

    <!-- BEGIN Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{URL::asset('public/assets/AdminTheme/assets/css/style.css')}}">
    <!-- END Custom CSS-->

    <style>
        .form-control-position
        {
            top:12px;
        }

        html body.bg-full-screen-image
        {
            background: url(../public/assets/myData/images/login-bg.jpg) !important;
        }

        @media screen and (max-width: 800px)
        {

        }
    </style>
</head>

<body class="vertical-layout vertical-menu-modern 1-column  bg-full-screen-image menu-expanded blank-page blank-page  pace-done" data-open="click" data-menu="vertical-menu-modern" data-col="1-column">

<div class="pace  pace-inactive">
    <div class="pace-progress" data-progress-text="100%" data-progress="99" style="transform: translate3d(100%, 0px, 0px);">
        <div class="pace-progress-inner"></div>
    </div>

    <div class="pace-activity"></div>
</div>

<!-- ////////////////////////////////////////////////////////////////////////////-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row"></div>

        <div class="content-body">
            <section class="flexbox-container">
                <div class="col-12 d-flex align-items-center justify-content-center">
                    <div class="col-md-4 col-10 box-shadow-2 p-0">
                        <div class="card border-grey border-lighten-3 px-1 py-1 m-0">
                            <div class="card-header border-0">
                                <div class="card-title text-center">
                                    <a ><img src="{{URL::asset("public/assets/AdminTheme/app-assets/images/logo/logo.png")}}" alt="Logo"></a><!-- change logo here -->
                                </div>

                                <h6 class="card-subtitle line-on-side text-muted text-center font-small-3 pt-2">
                                    <span>Forgot Password</span>
                                </h6>
                            </div>

                            <div class="card-content">
                                <div class="card-body" id="app">
                                    <form method="POST" action="{{ route('password.email') }}" class="form-horizontal" novalidate="">
                                        @csrf

                                        @if (session('status'))
                                            @roundalert_success
                                            @slot('text') {{ session('status') }} @endslot
                                            @endroundalert_success
                                        @endif

                                        <fieldset class="form-group position-relative has-icon-left">
                                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus placeholder="Your Email Address">
                                            @if ($errors->has('email'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                            <div class="form-control-position">
                                                <i class="la la-envelope info"></i>
                                            </div>
                                        </fieldset>

                                        <button type="submit" class="btn btn-outline-info btn-block"><i class="ft-unlock"></i> Send Password Reset Link</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

<!-- ////////////////////////////////////////////////////////////////////////////-->
<!-- BEGIN VENDOR JS-->
<script src="{{URL::asset('public/assets/AdminTheme/app-assets/vendors/js/vendors.min.js')}}" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->

<!-- BEGIN PAGE VENDOR JS-->
<script src="{{URL::asset('public/assets/AdminTheme/app-assets/vendors/js/forms/validation/jqBootstrapValidation.js')}}" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->

<!-- BEGIN MODERN JS-->
<script src="{{URL::asset('public/assets/AdminTheme/app-assets/js/core/app-menu.js')}}" type="text/javascript"></script>
<script src="{{URL::asset('public/assets/AdminTheme/app-assets/js/core/app.js')}}" type="text/javascript"></script>
<!-- END MODERN JS-->

<!-- BEGIN PAGE LEVEL JS-->
<script src="{{URL::asset('public/assets/AdminTheme/app-assets/js/scripts/forms/form-login-register.js')}}" type="text/javascript"></script>
<!-- END PAGE LEVEL JS-->

</body>
</html>