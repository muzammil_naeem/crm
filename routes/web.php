<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('auth.login');
//    return view('Layout.masterlayout');
});*/
Route::group(['namespace' => 'WebController'], function () {
    Route::get('Invoice/{id}','InvoiceController@viewInvoiceCustomer')->name('viewInvoiceCustomer')->middleware('guest');
    Route::get('downloadInvoicePdf/{id}','InvoiceController@downloadInvoicePdf')->name('downloadPdf')->middleware('guest');
    Route::post('acceptInvoiceCustomer/{id}','InvoiceController@acceptInvoiceCustomer')->name('acceptInvoiceCustomer')->middleware('guest');
    Route::get('getQuotaionRejectionForm/{id}','QuotationRejectionQuestions@getQuotaionRejectionForm')->name('getQuotaionRejectionForm')->middleware('guest');
    Route::post('postQuotaionRejectionForm/{id}','QuotationRejectionQuestions@postQuotaionRejectionForm')->name('postQuotaionRejectionForm')->middleware('guest');
    Route::get('reRequestForQuotation/{id}','RequestForQuotation@reRequestForQuotation')->name('reRequestForQuotation')->middleware('guest');
});

Auth::routes();

/*  Home Controller Get Routes   */
Route::get('/', 'HomeController@login')->name('customlogin')->middleware('guest');
Route::get('/home', 'HomeController@index')->name('home');

/*  Dashboard Controller Get Routes   */
Route::get('dummyMail','DashboardController@dummyMail')->middleware('guest');
Route::get('/', 'DashboardController@dashboard')->name('dashboard')->middleware('auth');
Route::get('/dashboardData', 'DashboardController@dashboardData')->middleware('auth');

Route::group(['namespace' => 'WebController','prefix' => '/salesperson'], function () {

    Route::resource('roles','RolesController',['except' => ['show']]);
    Route::resource('salesperson','SalesPersonController',['except' => ['show']]);
    Route::resource('customer','CustomerController',['except' => ['show']]);
    Route::resource('part','PartsController',['except' => ['show']]);
    Route::resource('activity_log','ActivityLogController',['except' => ['show']]);
    Route::resource('request_for_quote','RequestForQuotation');
    Route::resource('price_comparison','PriceComparisonController');
    Route::resource('account','AccountsController');
    Route::resource('lead','LeadsController');
    Route::resource('rejection_questions','QuotationRejectionQuestions');

    Route::get('expiredQuotations','QuotationController@expiredQuotations');
    Route::post('part/importParts','PartsController@importParts')->name('parts.import_parts');
    Route::get('part/downloadPartsCsv','PartsController@downloadPartsCsv')->name('parts.downloadPartsCsv');
    Route::get('getCustomer/{id}','GlobalController@getCustomer');
    Route::get('getPart/{id}','GlobalController@getPart');
    Route::post('createPriceComparison/{id}','PriceComparisonController@createPriceComparison');
    Route::get('reviewQuotation/{id}','PriceComparisonController@reviewQuotation');
    Route::post('submitQuotation/{id}','PriceComparisonController@submitQuotation');
    Route::post('assignSalesPerson/{id}','PriceComparisonController@assignSalesPerson');
    Route::get('sendInvoice/{id}','InvoiceController@sendInvoice');
    Route::get('listInvoices','InvoiceController@index');
    Route::post('customerFollowUp/{id}','InvoiceController@customerFollowUp');
    Route::get('followUpListing/{id}','InvoiceController@followUpListing');
    Route::get('pendingFollowups','InvoiceController@pendingFollowups');
    Route::get('acceptInvoiceSalesPerson/{id}','InvoiceController@acceptInvoiceSalesPerson');
    Route::get('downloadInvoicePdfSalesPerson/{id}','InvoiceController@downloadInvoicePdfSalesPerson');
    Route::get('downloadPurchaseOrder/{id}','InvoiceController@downloadPurchaseOrder');
    Route::post('uploadPurchaseOrder/{id}','InvoiceController@uploadPurchaseOrder');
    Route::get('markQuotationComplete/{id}','InvoiceController@markQuotationComplete');
    Route::get('rejection_questions/toggleStatus/{id}','QuotationRejectionQuestions@toggleStatus');

    //server side search
    Route::post('getCustomersAjax','GlobalController@getCustomersAjax');
    Route::post('getPartsAjax','GlobalController@getPartsAjax');
    Route::post('allPartsDatatable','GlobalController@allPartsDatatable');
    Route::post('activityLogDatatable','GlobalController@activityLogDatatable');
});

